<?php


class Order extends Eloquent  {

	
	protected $table = 'orders';

	
	public function client()
	{
		return $this->belongsTo('Client','client_id');
	}

	public function status()
	{
		return $this->belongsTo('OrderStatus','status_id');
	}

	public function credential()
	{
		return $this->belongsTo('Credential','credential_id')->with('elements.field');
	}
}
