<?php


class OrderStatus extends Eloquent  {

	
	protected $table = 'orderstatus';

	
	public function orders()
	{
		return $this->hasMany('Order','status_id');
	}
}
