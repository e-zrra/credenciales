<?php


class Client extends Eloquent  {

	
	protected $table = 'clients';

	
	public function user()
	{
		return $this->hasOne('User','client_id');
	}

	public function orders()
	{
		return $this->hasMany('Order','client_id');
	}

	public function category()
	{
		return $this->belongsTo('Category','category_id');
	}

	public function designs()
	{
		return $this->hasMany('Credential','client_id');

	}

	public static function SetCode()
	{

		$code = Client::max('id');
		$code++;

		switch ($code) {

			case ($code < 10 && $code > 0):
					$code = 'C000'.$code;
				break;
			case ($code < 100 && $code >= 10):
					$code = 'C00'.$code;
				break;
			case ($code < 1000 && $code >= 100):
				$code = 'C0'.$code;
				break;
			case ($code >= 1000):
				$code = 'C'.$code;
			break;
			
		}

		return $code;
	}

}

