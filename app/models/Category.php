<?php


class Category extends Eloquent  {

	
	protected $table = 'categories';

	
	public function clients()
	{
		return $this->hasMany('Client','category_id');
	}

	public function fields()
	{
		return $this->hasMany('Field','category_id');
	}
}
