<?php


class Field extends Eloquent  {

	
	protected $table = 'fields';

	public function category()
	{
		return $this->belongsTo('Category','category_id');
	}

	public function elements()
	{
		return $this->hasMany('Element','link_id');
	}
	
}
