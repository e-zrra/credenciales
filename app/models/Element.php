<?php


class Element extends Eloquent  {

	
	protected $table = 'elements';

	public function credential()
	{
		return $this->belongsTo('Credential','credential_id');
	}

	public function field()
	{
		return $this->belongsTo('Field','link_id');
	}

	
}
