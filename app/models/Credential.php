<?php


class Credential extends Eloquent  {

	
	protected $table = 'credentials';

	public function client()
	{
		return $this->belongsTo('Client','client_id');
	}

	public function elements()
	{
		return $this->hasMany('Element','credential_id');
	}

	public function orders()
	{
		return $this->hasMany('Order','credential_id');
	}


}
