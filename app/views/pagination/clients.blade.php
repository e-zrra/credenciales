@if($records->count() > 0)

	@foreach($records as $record)

		<div class="listview-outlook">
		    <a href="#" class="list" data-id="{{$record->id}}">
		        <div class="list-content">
		        	<span class="list-title">{{$record->name}}</span>
		        	<span class="list-subtitle">
		        		{{$record->address}}
		        		<span class="place-right">{{$record->category->description}}</span>
		        	</span>
		        	<span class="list-remark">{{$record->email}}</span>

		        </div>
		    </a>
		    	
		</div>

	@endforeach

@else
	
	<div class="notice marker-on-left">
     	No se Encontraron Registros
	</div>
	
@endif


