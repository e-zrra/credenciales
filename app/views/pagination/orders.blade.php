

@foreach($records as $record)
	<div class="listview-outlook">
	    <a href="#" class="list" data-id="{{$record->id}}">
	        <div class="list-content">
	        	<span class="list-title">
	        		{{$record->code}}
	        		<span class="place-right icon-flag-2 {{$record->status->class}} smaller"></span>
	        	</span>
	        	<span class="list-subtitle">
	        		{{ ($record->client) ? $record->client->name : '' }}
	        		<span class="place-right">
	        			{{ ($record->status) ? $record->status->description : '' }}
	        		</span>
	        	</span>
	        	<span class="list-remark">Credenciales: {{$record->noitems}}</span>
	        </div>
	    </a>	
	</div>
@endforeach

@if($records->count() == 0)
	
	<div class="notice marker-on-left">
     	No se Encontraron Registros
	</div>
	
@endif

