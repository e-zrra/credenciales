@include('pagination.paginator')

@if($records->count() > 0)

	@foreach($records as $record)
		<div><button data-id="{{$record->id}}" class="info showdesigner" data-action="update"><i class="icon-pencil"></i></button></div>

		<div class="row">
			<div class="span6">
				<div id="anversopreview" style="{{$record->A_style}};width:{{ $record->width }}px;height:{{ $record->height }}px;">
					<img id="background" style="width:100%;height:100%;position:absolute;z-indez:-1;" src="{{$record->A_background}}">
					@foreach($record->elements as $element)

						@if($element->view == 'anverso')
							@if($element->data_type == 'text')
								<span style="{{$element->style}}">{{$element->textcontent}}</span>
							@endif

							@if($element->data_type == 'image')
								<span style="{{$element->style}}"><img style="width:100%;height:100%;" src="{{$element->imagecontent}}"></span>
							@endif

							@if($element->data_type == 'barcode')
								<span style="{{$element->style}}"><img style="width:100%;height:100%;" src="../public/img/barcode.jpg"></span>
							@endif
						@endif

					@endforeach
				</div>

			</div>
			<div class="span6">
				<div id="reversopreview" style="{{$record->R_style}};width:{{ $record->width }}px;height:{{ $record->height }}px;">
					<img id="background" style="width:100%;height:100%;position:absolute;z-indez:-1;" src="{{$record->R_background}}">
					@foreach($record->elements as $element)

						@if($element->view == 'reverso')
							@if($element->data_type == 'text')
								<span style="{{$element->style}}">{{$element->textcontent}}</span>
							@endif

							@if($element->data_type == 'image')
								<span style="{{$element->style}}"><img style="width:100%;height:100%;" src="{{$element->imagecontent}}"></span>
							@endif

							@if($element->data_type == 'barcode')
								<span style="{{$element->style}}"><img style="width:100%;height:100%;" src="../public/img/barcode.jpg"></span>
							@endif
						@endif

					@endforeach
				</div>
			</div>
		</div>

	@endforeach

@else
	
	<div class="notice">No se encontraron diseños para mostrar</div>

@endif
