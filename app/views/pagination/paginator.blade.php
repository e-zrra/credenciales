
@if ($records->getLastPage() > 1)
 	<div class="pagination">
		<ul>

			<li class="{{ ($records->getCurrentPage() == 1) ? 'disabled' : '' }}">
				<a class="paglink" href="{{ $records->getUrl(1) }}" >
					<i class="icon-previous"></i>
				</a>
			</li>
			@for ($i = 1; $i <= $records->getLastPage(); $i++)
				<li class="{{ ($records->getCurrentPage() == $i) ? ' disabled' : '' }}">
					<a class="paglink" href="{{ $records->getUrl($i) }}">
						{{ $i }}
					</a>
				</li>
			@endfor
			<li class="{{ ($records->getCurrentPage() == $records->getLastPage()) ? ' disabled' : '' }}">
				<a class="paglink" href="{{ $records->getUrl($records->getLastPage()+1) }}" > 
					<i class="icon-next"></i>
				</a>
			</li>
		</ul>
	</div>
 
@endif



