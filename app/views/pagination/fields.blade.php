@foreach($fields as $field)
	<tr data-id="{{$field->id}}">
		
		<td>{{$field->displayname}}</td>
		<td>{{$field->type}}</td>
		<td>
			<button class="warning small editfield"><i class="icon-pencil"></i></button>
			<button class="primary small updatefield"><i class="icon-floppy"></i></button>
		</td>

	</tr>

@endforeach