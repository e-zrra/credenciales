@extends('masterpageadmin')


@section('content')
	
	<nav class="breadcrumbs small">
	    <ul>
	        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
	        <li class="active"><a href="#">Configuración</a></li>
	    </ul>
	</nav>

	<div style="margin-top:4%;"></div>

	<div class="accordion" data-role="accordion">
	    <div class="accordion-frame">
	        <a href="#" class="heading">Categorías y Campos Variables</a>
	        <div class="content">
	        	
	        	<div class="grid">
					<div class="row">

						<div class="span6">
							<div id="categories">
								<button id="show-category-form" data-action="Registrar">Nueva</button>
								<table class="table hovered">
									<thead>
										<th>Categoría</th>
										<th></th>
									</thead>
									<tbody>
										@foreach($categories as $category)
											<tr data-id="{{$category->id}}">
												<td>{{$category->description}}</td>
												<td>
													<button class="warning small editcategory" data-hint="Categoría|Modificar Nombre de la Categoría" data-hint-position="top"><i class="icon-pencil"></i></button>
													<button class="primary small updatecategory" data-hint="Categoría|Guardar Cambios" data-hint-position="top"><i class="icon-floppy"></i></button>
													<button class="success small fieldscategory" data-hint="Campos|Mostrar la lista de campos de la categoría" data-hint-position="top"><i class="icon-list"></i></button>
												</td>
											</tr>

										@endforeach

									</tbody>

								</table>
							</div>

							<div id="category" style="display:none;">

								@include('forms.category')

							</div>

						</div>

						<div class="span6">

							<div id="fields" style="display:none;" >
								<button id="show-field-form" data-action="Registrar">Nuevo</button>
								<table  class="table striped">
									<thead>
										<tr>
											
											<th>Descripción</th>
											<th>Tipo</th>
											<th></th>
										</tr>
									</thead>
									<tbody id="categoryfields">
										
									</tbody>
								</table>

							</div>

							<div id="field" style="display:none;">

								@include('forms.field')

							</div>
						</div>
					</div>
				</div>


	        </div>
	    </div>
	    <div class="accordion-frame">
	    	<a href="#" class="heading">Emails y Notificaciones</a>
	    	<div class="content">
	    		<div class="grid">
	    			<div class="grid">
	    				<div class="row">
	    					<div class="span4">
	    						<label>Correo Electrónico donde se recibiran las notificaciones cuando:<br> El cliente solicite servicio<br> El cliente agregue registros a la solicitud</label>
	    						<div class="input-control text">
								    <input name="adminemail" type="text" value="{{$config->adminemail}}"/>
								    <button class="btn-clear"></button>
								</div>
								<button id="saveadminemail" class="default">Guardar</button>

	    					</div>
	    					<div class="span4">
	    						<div class="input-control switch">
								    <label>
								    	Permitir al cliente escojer el diseño de la credencial de la solicitud.<br>
								    	@if($config->allowdesigner == 1)
								        	<input name="allowdesigner" type="checkbox" checked="checked" />
								        @else
								        	<input name="allowdesigner" type="checkbox" />
								        @endif
								        <span class="check"></span>
								    </label>
								</div>
	    					</div>
	    					<div class="span4">
	    						<div class="input-control switch">
								    <label>
								    	Enviar Email al recipiente del cliente notificando la aprobación de su solicitud.<br>
								    	@if($config->emailaprovee == 1)
								        	<input name="aproveeorder" type="checkbox" checked="checked" />
								        @else
								        	<input name="aproveeorder" type="checkbox" />
								        @endif
								        <span class="check"></span>
								    </label>
								</div>

	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	   
	</div>


@endsection


@section('scripts')

	<script>
		$(document).ready(function(){

			catselected = 0 ;

			$('#categories tr').css('cursor','pointer');

			$('input[name=aproveeorder]').click(function(){

				var data;

				if($(this).is(':checked'))
				{
					data = {aprovee:1};

				}
				else
				{
					data ={aprovee:0};
				}

				var result = ajaxCall('post','configuration/emailaprovee',data);

				$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

					});
			})

			$('input[name=allowdesigner]').click(function(){

				var data;

				if($(this).is(':checked'))
				{
					data = {allow:1};

				}
				else
				{
					data ={allow:0};
				}

				var result = ajaxCall('post','configuration/allowdesigner',data);

				$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

					});
			})

			$('#saveadminemail').click(function(){

				var data = {adminemail:$('input[name=adminemail]').val()};

				var result = ajaxCall('post','configuration/updateadminemail',data);

				$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

				});


			});


			$('#fieldtype').change(function(){

				$('#' + $(this).val()).show();
			    $('#' + $(this).find(':selected').attr('data-hide')).hide();
			})

			$('#show-category-form').click(function(){

				$('#categories').hide();
				$('#category').show(600);
				$('#action-category').text($(this).attr('data-action'));
				$('#action-category').attr('data-action','save');

			});

			$('#show-field-form').click(function(){

				$('#fields').hide();
				$('#field').show(600);
				$('#action-field').text($(this).attr('data-action'));
				$('#action-field').attr('data-action','save');


			});

			$('.cancel').click(function(){

				$('#' + $(this).attr('data-target')).hide();
				$('#' + $(this).attr('data-show')).show(600);


			});

			$('#action-category').click(function(){

				var data = $('#form-category').serializeArray();
				var result;

				if($(this).attr('data-action') == 'save')
				{
					result = ajaxCall('post','configuration/savecategory',data);
				}
				else
				{

				}

				
				if(result.success == true)
				{
					$('#form-category').hide();
					$('#categories').show(600);
					$('#categories > table > tbody').append('<tr data-id="' + result.category.id  +'"><td>' + result.category.description + '</td><td><button class="warning small editfield"><i class="icon-pencil"></i></button></td></tr>')
				}

				$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

				});
				

			});

			$('#action-field').click(function(){

				var data = $('#form-field').serializeArray();
				var result;

				data.push({name:'category',value:catselected});

				if($(this).attr('data-action') == 'save')
				{
					result = ajaxCall('post','configuration/savefield',data);
				}
				else
				{

				}

				if(result.success == true)
				{
					$('#form-field').hide();
					$('#fields').show(600);

					$('#fields > table > tbody').append('<tr data-id="' + result.field.id  +'"><td>' + result.field.displayname + '</td><td>' + result.field.type +'</td><td><button class="warning small editcategory"><i class="icon-pencil"></i></button></td></tr>')
				}

				$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

				});
				

			});



			$(document).on('click','.editcategory',function(){

				var tr = $(this).parent().parent();

				var td = tr.find('td:eq(0)');

				var text = td.text();

				var input = td.find('input');

				if(input.length > 0)
				{
					td.html(input.val());
				}
				else
				{
					td.html('<input type="text" value="'+text+'">');
				}


			});

			$(document).on('click','.editfield',function(){

				var tr = $(this).parent().parent();

				var td = tr.find('td:eq(0)');

				var text = td.text();

				var input = td.find('input');

				if(input.length > 0)
				{
					td.html(input.val());
				}
				else
				{
					td.html('<input type="text" value="'+text+'">');
				}


			});

			$(document).on('click','.updatecategory',function(){

				var tr = $(this).parent().parent();

				var td = tr.find('td:eq(0)');

				var input = td.find('input');

				if(input.length > 0)
				{
					data = {category: tr.attr('data-id'),value:input.val() };
					var result = ajaxCall('post','configuration/updatecategory',data);

					if(result.success == true)
					{
						td.html(result.value);
					}

					$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

					});
				
				}
				

			});

			$(document).on('click','.updatefield',function(){

				var tr = $(this).parent().parent();

				var td = tr.find('td:eq(0)');

				var input = td.find('input');

				if(input.length > 0)
				{
					data = {field: tr.attr('data-id'),value:input.val() };
					var result = ajaxCall('post','configuration/updatefield',data);

					if(result.success == true)
					{
						td.html(result.value);
					}

					$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

					});
				
				}
				

			});


			$(document).on('click','.fieldscategory',function(){

				if(catselected != $(this).attr('data-id'))
				{

					$('.selected').removeClass('selected');

					$(this).parent().parent().addClass('selected');

					$('#fields').show();

					getFields($(this).parent().parent().attr('data-id'))
					catselected = $(this).parent().parent().attr('data-id');
					$('#field').hide();
				}
			})

			function getFields(category)
			{

				var data = {category:category};

				var result = ajaxCall('get','configuration/categoryfields',data);

				$('#categoryfields').html(result.view);


			}

			function ajaxCall(type,url,data)
			{
				var result;

				$.ajax({
						type: type,
						url: url,
						data: data,
						async:false
				})
			  	.done(function( msg ) {
			    	
			  		result = msg;

			  	});

			  	return result;

			}

		});

	</script>


@endsection
