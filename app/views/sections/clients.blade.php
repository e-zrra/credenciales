@extends('masterpageadmin')

@section('css')
	
	{{HTML::style('css/jquery-ui.css')}}
	{{HTML::style('css/colpick.css')}}
	
@endsection


@section('content')
	
	<nav class="breadcrumbs small">
	    <ul>
	        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
	        <li class="active"><a href="#">Clientes</a></li>
	    </ul>
	</nav>

	

	<div id="main">
		@if(Auth::user()->hasRole('SUPERADMIN'))
		<h2 class="subheader">Clientes Registrados</h2>
		<button class="default" id="new">Nuevo</button>

		<div style="margin-bottom:0%;"></div>

		<div class="grid">
			<div class="row">
				<div class="span2">
					
					<div class="input-control select">
					    <select id="category-search">
					    	<option value="">Categoría</option>
					    	@foreach($categories as $category)
					    		<option value="{{$category->id}}">{{$category->description}}</option>
					    	@endforeach
					    	
					    </select>
					</div>
					
				</div>
				<div class="span4 offset3">
					<div class="input-control text size3">
					    <input type="text" id="text-search" placeHolder="Busqueda"/>
					    <button class="btn-search"></button>
					</div>
				</div>
			</div>
		</div>
		@endif


		<div class="grid" >
			<div class="row">
				<div class="span8">

					
					<div id="clients" style="height:310px;overflow-y: auto;">

					</div>


				</div>
				<div class="span4">

					<div id="details" style="display:none;">
						<div style="margin-top:0%;"></div>
						
						<div class="toolbar fg-red">
						    <div class="toolbar-group">
						    	@if(Auth::user()->hasRole('SUPERADMIN'))
						        	<button class="primary" id="client-edit" data-hint="Editar|Cambiar la información del cliente" data-hint-position="top"><i class="icon-pencil fg-white"></i></button>
						        	<button class="danger" id="client-remove" data-hint="Eliminar|Eliminar al cliente" data-hint-position="top"><i class="icon-remove"></i></button>
						       	@endif

						       	@if(Auth::user()->hasRole('SUPERADMIN') || $config->allowdesigner == 1)
						       		<button class="warning" id="client-designs" data-hint="Diseños|Diseños de credenciales" data-hint-position="top"><i class="icon-credit-card"></i></button>
						       	@endif
						    </div>
						   
						</div>

						<div style="margin-top:8%;"></div>
						<p class="subheader-secondary" id="client-code"></p>
						<div id="client-name"></div>
						<div id="client-address"></div>
						<div id="client-email"></div>
						<div id="client-phone"></div>
						<div id="client-dateadded"></div>

						<div style="margin-top:5%;"></div>
						<div id="client-username"></div>

						<div style="margin-top:5%;"></div>
						<div id="client-category"></div>

					</div>

				</div>
			</div>
		
		</div>
	</div>

	<div class="grid" id="designs" style="display:none;">
		<div class="row">
			<div class="span12">
				<h2 id="client-name"></h2>
				<h2><a href="#" class="backtomain"><i class="icon-arrow-left-3 fg-darker"></i></a> Diseños</h2>
				<button class="default showdesigner" data-action="save">Nuevo</button>

				<div style="margin-top:2%;"></div>

				<div id="designscontent">

				</div>
				
			</div>
		</div>
	</div>

	<div id="designer" style="display:none;">
		@include('partials.designer')
	</div>


	<div class="grid" id="client" style="display:none;">
		<div class="row">
			<div class="span12">

				@include('forms.client')

			</div>
		</div>

	</div>

@endsection

@section('scripts')
	
	{{HTML::script('js/jquery-ui.min.js')}}
	{{HTML::script('js/colpick.js')}}
	{{HTML::script('js/clients/clients-handler.js')}}
	{{HTML::script('js/credential/designer.js')}}
	
@endsection