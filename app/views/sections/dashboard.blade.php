@extends('masterpageadmin')

@section('content')
	

	<h2 class="subheader">Dashboard</h2>
	<div style="margin-bottom:7%;"></div>

	@if(Auth::user()->hasRole('SUPERADMIN') || $config->allowdesigner == 1)
		<a href="{{URL::route('clients')}}">
			<div class="tile bg-darkPink">
			    <div class="tile-content icon">
			        <i class="icon-user-3"></i>
			    </div>
			    <div class="tile-status">
			        <span class="name">Clientes</span>
			    </div>
			</div>
		</a>
	@endif

	<a href="{{URL::route('orders')}}">
		<div class="tile bg-darkCyan">
		    <div class="tile-content icon">
		        <i class="icon-grid-view"></i>
		    </div>
		    <div class="tile-status">
		        <span class="name">Solicitudes</span>
		    </div>
		</div>
	</a>

	@if(Auth::user()->hasRole('SUPERADMIN'))
		<a href="{{URL::route('configuration')}}">
			<div class="tile bg-darkGreen">
			    <div class="tile-content icon">
			        <i class="icon-tools"></i>
			    </div>
			    <div class="tile-status">
			        <span class="name">Configuración</span>
			    </div>
			</div>
		</a>
	@endif


@endsection