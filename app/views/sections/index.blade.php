@extends('masterpage')



@section('content')
	
	<div class="grid fluid">

		<div class="row">
			<div class="span4 offset4">

				@include('forms.login')

			</div>
		</div>

	</div>
	
	
@endsection

@section('scripts')
	
	<script>
		
		$(document).ready(function(){

			$('#trigger-login').click(function(){

				var data 	= $('#form-login').serializeArray();
				var result  = ajaxCall('post','login',data);

				if(result.success == false)
				{
					

					$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

					});

				}
				else
				{
					window.location.href = result.redirect;
				}

			});


			function ajaxCall(type,url,data)
			{
				var result;

				$.ajax({
						type: type,
						url: url,
						data: data,
						async:false
				})
			  	.done(function( msg ) {
			    	
			  		result = msg;

			  	});

			  	return result;

			}


		});


	</script


@endsection