@extends('masterpageadmin')
@section('css')

{{HTML::style('css/colpick.css')}}
{{HTML::style('css/jquery-ui.css')}}

<style>

	#credentialspreview {
	  background: rgb(204,204,204); 		  
	}

	@page {
	    size: 297mm 210mm; /* landscape */
	}

	page[size="A4"] {
	  background: white;
	  width: 21cm;
	  height: 27cm;
	  display: block;
	  margin: 0 auto;
	  margin-bottom: 0.5cm;
	  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);

	}
	@page {
	    size: auto;
	    margin: 0px;
	    width:100%;
	    float: none; 
	}
</style>
@endsection

@section('content')
	
	<nav class="breadcrumbs small">
	    <ul>
	        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
	        <li class="active"><a href="#">Solicitudes</a></li>
	    </ul>
	</nav>

	<h2 class="subheader">Solicitudes</h2>
	<button class="default" id="new">Nueva</button>

	<!--<div style="margin-bottom:2%;"></div>-->

	<div id="main">
		<div class="grid">
			<div class="row">
				<div class="span2">
					<div class="input-control select">
					    <select id="status-search">
					    	<option value="">Estado</option>
					    	
					    	@foreach($status as $s)
					    		<option value="{{$s->id}}">{{$s->description}}</option>
					    	@endforeach
					       
					    </select>
					</div>
				</div>
				<div class="span4 offset3">
					<div class="input-control text size3">
					    <input type="text" id="text-search" placeHolder="Busqueda"/>
					    <button id="ordersearch" class="btn-search"></button>
					</div>
				</div>
			</div>
		</div>

		<div class="grid">
			<div class="row">
				<div class="span8">

					<div id="orders" style="height:310px;overflow-y: auto;">

					</div>


				</div>
				<div class="span4" style="width:40% !important;">

					<div id="details" style="display:none;">
						<div style="margin-top:0%;"></div>
						
						<div class="toolbar fg-red">

							@if(Auth::user()->hasRole('SUPERADMIN'))
							    <div class="toolbar-group">
							    	<button class="success" id="order-approve" data-hint="Aprobar|Aceptar Solicitud" data-hint-position="top"><i class="icon-checkmark fg-white"></i></button>
							        <button class="primary" id="order-edit" data-hint="Modificar|Editar datos de Solicitud" data-hint-position="top"><i class="icon-pencil fg-white"></i></button>
							       	<button class="inverse" id="order-cancel" data-hint="Cancelar|Cancelar Solicitud" data-hint-position="top"><i class="icon-cancel-2 fg-white"></i></button>
							     	
							    </div>
						    @endif

						   <div class="toolbar-group">
						    	
						        <button class="warning" id="order-credential" data-hint="Credencial|Mostrar Diseño de la Credencial" data-hint-position="top"><i class="icon-credit-card"></i></button>
						        <button class="info" id="order-records" data-hint="Registros|Mostrar los Registros de la Solicitud" data-hint-position="top"><i class="icon-folder"></i></button>
						    </div>

						    @if(Auth::user()->hasRole('SUPERADMIN'))
							    <div class="toolbar-group">
							    	
							         <button class="danger" id="order-remove" data-hint="Eliminar|Eliminar Solicitud" data-hint-position="top"><i class="icon-remove"></i></button>
							       
							    </div>
						    @endif
						   
						</div>

						<div style="margin-top:8%;"></div>
						<p class="subheader-secondary" id="order-code"></p>
						<div id="order-noitems"></div>
						<div id="order-itemsremaining"></div>
						<div id="order-client"></div>
						<div id="order-duedate"></div>
						<div id="order-dateadded"></div>

						<div style="margin-top:5%;"></div>
						<div id="order-notes"></div>
						

					</div>

					<div id="credential" style="display:none;">
						<p class="subheader"><a id="backtodetails" href="#" style="cursor:pointer;"><i class="icon-arrow-left-3 fg-darker smaller"></i></a> Credencial</p>
						

						<div style="margin-bottom:2%;"></div>

						<div class="toolbar fg-white">
							<div>
								@if($config->allowdesigner == 1 || Auth::user()->hasRole('SUPERADMIN'))
									<div class="fg-black" id="alertcredential"></div><button id="choosedesign" class="default mini"> Elegir Diseño</button>
							   	@endif
							    <div class="toolbar-group">
							    <br>
							        <button class="warning showpreview"  data-target="anverso" data-hide="reverso" data-hint="Anverso|Previsualización" data-hint-position="top"><i class="icon-credit-card"></i></button>
							        <button class="inverse showpreview" data-target="reverso" data-hide="anverso" data-hint="Reverso|Previsualización" data-hint-position="top"><i class="icon-credit-card"></i></button>
							        
							    </div>
							    
						    </div>

						</div>

						<div style="margin-bottom:2%;"></div>

						<div id="previews">
							<div id="anverso" style="position:relative;margin:0px auto;width:325.039px;height:204.094px;border:1px #d9d9d9 solid;-webkit-print-color-adjust: exact; "><img id="background" style="position:absolute;width:100%;height:100%;" /></div>
							<div id="reverso" style="position:relative;margin:0px auto;width:325.039px;height:204.094px;border:1px #d9d9d9 solid;display:none;"><img id="background" style="position:absolute;width:100%;height:100%;" /></div>
						</div>
						<div id="previews-inverso">
							<div id="reverso" style="position:relative;margin:0px auto;width:325.039px;height:204.094px;border:1px #d9d9d9 solid;display:none;"><img id="background" style="position:absolute;width:100%;height:100%;" /></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	

	<div id="orderrecords" style="display:none;">
		 <h2 class="subheader"><a href="#" class="backtomain" style="cursor:pointer"><i class="icon-arrow-left-3 fg-darker smaller"></i></a> Registros</h2>
		<div class="toolbar">
		 	<div class="toolbar-group">
		 		
				<a href="{{URL::route('DownloadExcel')}}" class="button" id="downloadexcel" data-hint="Descargar|Descargar Plantilla Archivo Excel" data-hint-position="top"><i class="icon-file-excel" ></i></a>
		 		<button id="uploadexcel" data-hint="Cargar|Ingresar Registros mediante Archivo Excel" data-hint-position="top"><i class="icon-upload-3" ></i></button>
				<input name="excelfile" id="excelfile" type="file"   tabindex="-1" style="z-index:0;display:none;">
				
				@if(Auth::user()->hasRole('SUPERADMIN'))
					<button id="generatecredentials" data-hint="Generar|Generar Credenciales para Imprimir" data-hint-position="top"><i class="icon-credit-card" ></i></button>
					<button id="markascompleted" data-hint="Completadas|Marcar como completada" data-hint-position="top"><i class="icon-checkmark"></i></button>
				@endif
			</div>
		</div>
		<div id="pag">
		</div>
		<div class="grid">
			<div class="row">
				<div class="span1">
					<div class="input-control select">
					    <select id="recordperpage" name="recordperpage">

					        <option value="30">30</option>
					        <option value="50">50</option>
					        <option value="100">100</option>
					    </select>
					</div>
				</div>
				<div class="span2">
					<div class="input-control select">
					    <select id="recordstatus" name="recordstatus">
					        <option value="0">Pendiente</option>
					        <option value="1">Entregada</option>
					     
					    </select>
					</div>
				</div>
				<div class="span3 offset8">
					<div class="input-control text">
					    <input type="text" name="recordsearch" />
					    <button id="recordsearch" class="btn-search"></button>
					</div>

				</div>
			</div>
		</div>
		
		<div id="tablerecords">
		<button type="button" id="" class="show-data-load primary">Insertar imagenes</button>
		<button type="button" id="" class="save-data-load primary">Guardar registros</button>
		<button type="button" id="btn-setting-codebar" class="primary">Configurar codigo de barras</button>	
		<div class="hidden well" id="content-setting-codebar">
			<h4>Configurar codigo de barras</h4>
			<ul>
				
			</ul>
		</div>
			<table class="table striped condensed" style="text-align: left;">
				<thead></thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="span16">
			<div id="" class="generatedcredentials generatedcredentials-anverso" style="display:none;">
				<h3 class="subheader">
					<a href="#" class="backtoorderrecords" style="cursor:pointer">
						<i class="icon-arrow-left-3 fg-darker smaller"></i>
					</a><br />
					Previsualización Anverso
				</h3>
				<button id="printcredentials">Imprimir</button>

				<div class="grid"  style="height:800px;overflow-y : scroll;border: 1px solid #ddd;">
					<div class="row">
						<div class="span12" class="" id="credentialspreview">
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--<div class="span16">
			<div id="" class="generatedcredentials generatedcredentials-inverso" style="display:none;">
				<h3 class="subheader">
					Previsualización Reverso
				</h3>
				<button id="printcredentials-inverso">Imprimir</button>

				<div class="grid"  style="height:300px;overflow-y : scroll;border: 1px solid #ddd;">
					<div class="row">
						<div class="span12" class="" id="credentialspreview-inverso">
						</div>
					</div>
				</div>
			</div>
		</div>-->

	</div>
	

	<div id="camera" style="display:none;">

		
	</div>

	<div class="grid" id="order" style="display:none;">
		<div class="row">
			<div class="span12">

				@include('forms.order')

			</div>
		</div>

	</div>

	<div id="dialog" style="display:none;">
  		<div style="width:100%;height:100%;">
  			<div id="photo" style="position:relative;margin:0px auto;width:250px;height:200px;margin-top:35px;">
  				<h3>Fotografía</h3>
  			</div> 
  			<div style="position:relative;margin:0px auto;width:250px;margin-top:35px;">
  				<button class="primary" id="closephoto" >Cerrar</button>
  			</div>
  		</div>
	</div>

	@if($config->allowdesigner == 1 || Auth::user()->hasRole('SUPERADMIN'))
		<div id="choosedesigndialog" style="display:none;">
	  		<div id="designscontent" class="grid">

			</div>

		</div>
	@endif

@endsection

@section('scripts')
	
	{{HTML::script('js/colpick.js')}}
	{{HTML::script('js/jquery-ui.min.js')}}
	{{HTML::script('js/photobooth_min.js')}}
	{{HTML::script('js/jQuery.print.js')}}
	{{HTML::script('js/CODE128.js')}}
	{{HTML::script('js/JsBarcode.js')}}
	{{HTML::script('js/orders/orders-handler.js')}}

@endsection