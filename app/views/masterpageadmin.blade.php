<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Application</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	</head>
	{{HTML::style('css/metro-bootstrap.css')}}
	{{HTML::style('css/metro-bootstrap-responsive.css')}}
	{{HTML::style('css/iconFont.min.css')}}
	@yield('css')
	<body class="metro">
		<header class="bg-dark">
			@include('partials.nav-menu')
		</header>
		<div class="container">
			@yield('content')
		</div>
		{{HTML::script('js/jquery-1.11.2.min.js')}}
		{{HTML::script('js/jquery.widget.min.js')}}
		{{HTML::script('js/jquery.mousewheel.js')}}
		{{HTML::script('js/metro.min.js')}}
		@yield('scripts')
	</body>
</html>