<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Your Website</title>
	</head>

	<!--checar este layout-->

	{{HTML::style('css/metro-bootstrap.css')}}
	{{HTML::style('css/metro-bootstrap-responsive.css')}}
	{{HTML::style('css/iconFont.min.css')}}

	@yield('css')

	<body class="metro">

		
		@yield('content')

		{{HTML::script('js/jquery-1.11.2.min.js')}}
		{{HTML::script('js/jquery.widget.min.js')}}
		{{HTML::script('js/jquery.mousewheel.js')}}
		{{HTML::script('js/metro.min.js')}}

		@yield('scripts')

	</body>




</html>