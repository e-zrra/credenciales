<nav class="navigation-bar">
    <nav class="navigation-bar-content container">
        <div class="element">
            <a class="dropdown-toggle" href="#">ARCHIVO</a>
            <ul class="dropdown-menu" data-role="dropdown">
                <li><a href="#">Mi Cuenta</a></li>
                @if(Auth::user()->hasRole('SUPERADMIN'))
                    <li><a href="#">Administrador de Usuarios</a></li>
                @endif
                <li class="divider"></li>
                <li><a href="{{URL::to('logout')}}">Salir</a></li>
            </ul>
        </div>
 
        <span class="element-divider"></span>
        

        <button class="element image-button image-left place-right">
            {{Auth::user()->username}}
            
        </button>
    </nav>
</nav>