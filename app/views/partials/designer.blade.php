<h2><a id="backtodesigns" href="#"><i class="icon-arrow-left-3 fg-darker"></i></a> Diseñador</h2>
<div class="button-set" data-role="button-group">
	<button class="savedesign success" data-hint="Guardar|Guardar Cambios del Diseño" data-hint-position="top"><i class="icon-floppy"></i></button>
</div>
<div class="grid">
	<div class="row">
		<div class="span4">
			<fieldset>
				<legend>Elementos</legend>
				<div class="button-set" data-role="button-group">

				    <button class="text-tools option" data-type="text" data-target="text-tools" data-hint="Texto|Agregar a Lienzo" data-hint-position="top"><i class="icon-type"></i></button>
				    <button class="option" data-type="image" data-target="image-tools" data-hint="Imagen|Agregar a Lienzo" data-hint-position="top"><i class="icon-pictures"></i></button>
				    <button class="option" data-type="barcode" data-target="barcode-tools" data-hint="Código de Barras|Agregar a Lienzo" data-hint-position="top"><i class="icon-barcode"></i></button>
				    <button class="option" data-selectedview="anversodesigner" data-target="view-tools" data-hint="Anverso|Anverso del Diseño" data-hint-position="top"><i class="icon-credit-card"></i></button>
				    <button class="option" data-selectedview="reversodesigner" data-target="view-tools" data-hint="Reverso|Reverso del Diseño" data-hint-position="top"><i class="icon-credit-card"></i></button>
					<button class="removeelement danger" data-hint="Remover|Remover Elemento del Diseño" data-hint-position="top"><i class="icon-remove"></i></button>
					
				    
				</div>

				<div style="margin-bottom:12%;"></div>

				<div id="tools">
					<div id="text-tools" style="display:none;">
						<div class="row">
							<div class="span4">
								<div class="input-control text" data-hint="Texto" data-hint-position="top">
								    <input type="text" id="element-text" value="Texto"/>
								    <button class="btn-clear"></button>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="span4">
								<div class="input-control select" id="element-font-family" data-hint="Tipo de Letra" data-hint-position="top">
								    <select>
								        <option value="Arial">Arial</option>
								        <option value="Tahoma">Tahoma</option>
								        <option value="Impact">Impact</option>
								        <option value="Times New Roman">Times New Roman</option>
								    </select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="span1">
								<div class="input-control text" data-hint="Tamaño" data-hint-position="top">
								    <input type="number" id="element-text-size" value="10" min="1" />
								    
								</div>
							</div>
							<div class="span1">
								<div id="text-color" data-hint="Color" class="selectablecolor" data-target="text-color" data-hint-position="top" style="border:1px solid #d9d9d9;width:100%;height:35px;background-color:black;">
								    
								</div>
							</div>
							<div class="span2">
								<button class="font-style" data-css="font-weight" data-value="bold" data-hint="Estilo|Negrita" data-hint-position="top">B</button>
							    <button class="font-style" data-css="font-style" data-value="italic" data-hint="Estilo|Itálica" data-hint-position="top">I</button>
							    <button class="font-style" data-css="text-decoration" data-value="underline" data-hint="Estilo|Subrayado" data-hint-position="top">U</button>
							</div>
						</div>


					</div>

					<div id="image-tools" style="display:none;">

						<button class="file-action" data-target="element-file">Establecer Imagen</button>
						<input type="file" class="file-selected" id="element-file" data-type="element" data-target="element-image-preview" style="display:none;">

						<div  style="width:134px;height:126px;border:1px solid #d9d9d9;">
							<img id="element-image-preview" style="width:100%;height:100%;" />
						</div>

						

					</div>

					<div id="view-tools" style="display:none;">
						<div class="row">
							<div class="span2">

								<button class="file-action" data-target="view-file">Establecer Imagen</button>
								<input type="file" class="file-selected" id="view-file" data-type="view" data-target="view-image-preview" style="display:none;">

								<div  style="width:134px;height:126px;border:1px solid #d9d9d9;">
									<img id="view-image-preview" style="width:100%;height:100%;" />
								</div>

								<button data-target="view-image-preview" class="danger removeimage">Remover</button>

							</div>
							<div class="span2">
								<div id="view-color" data-target="view-color" data-hint="Color de Fondo" class="selectablecolor" data-hint-position="top" style="border:1px solid #d9d9d9;width:100%;height:35px;">
								    
								</div>
							</div>
						</div>
					</div>

					<div id="barcode-tools">
					</div>
				</div>

			</fieldset>
		</div>

		<div class="span5">
			<fieldset>
				<legend>Diseño</legend>
				<div>
					<button class="option credential-vertical" data-type="format-position-vertical" data-target="format-position-vertical" data-hint="Posición Vertical" data-hint-position="top">Vertical</button>
				    <button class="option credential-horizontal" data-type="format-position-horizontal" data-target="format-position-horizontal" data-hint="Posición Horizontal" data-hint-position="top">Horizontal</button>
					<input type="hidden" name="" class="input-credencial-width" value="325.039" >
					<input type="hidden" name="" class="input-credencial-height" value="204.094" >
				</div>
				<div class="row">
					<div class="col-md-12">
						<div id="canvas">
							<div id="anversodesigner" data-target="view-tools" class="view" style="margin: 0px auto; position: relative; width: 325.039px; height: 204.094px;">											
								<img id="background" style="width:100%;height:100%;position:absolute;z-indez:-1;">
							</div>
							
							<div style="margin-bottom:3px;"></div>

							<div id="reversodesigner" data-target="view-tools" class="view" style="margin: 0px auto; position: relative; width: 325.039px; height: 204.094px;">											
								<img id="background" style="width:100%;height:100%;position:absolute;z-indez:-1;">
							</div>
						</div>
					</div>
				</div>
				

			</fieldset>
		</div>
		<div class="span3">
			<fieldset>
				<legend>Elementos Variables</legend>
				@foreach($fields as $field)
					<button style="margin:2px;" data-fieldname="{{$field->displayname}}" data-target="{{$field->datatarget}}" data-type="{{$field->type}}" data-id="{{$field->id}}" class="primary option">
						{{$field->displayname}}
					</button>
					<br>
				@endforeach
			</fieldset>
		</div>
		
	</div>

	
</div>
