<form id="form-field">

	<div class="grid">
		<div class="row">
			<div class="span6">

				<fieldset>
					<legend>Campo</legend>
					<label>Nombre de la Campo</label>
					<div class="input-control text size4">
					    <input type="text" value="" name="field"/>
					    <button class="btn-clear"></button>
					</div>

					<label>Tipo de Campo</label>
					<div class="input-control select size4" >
					    <select id="fieldtype" name="fieldtype">
					        <option value="text" data-hide="image" >Texto</option>
					        <option value="image" data-hide="text">Imagen</option>
					     
					    </select>
					</div>
					<br>

					<div id="text" class="input-control checkbox">
					    <label>
					        <input type="checkbox" name="barcode" />
					        <span class="check"></span>
					        Generador de Código de Barras
					    </label>
					</div>
					

					<div id="image" class="input-control checkbox" style="display:none;">
					    <label>
					        <input type="checkbox"  name="photo"/>
					        <span class="check"></span>
					        Contenedor de Fotografía
					    </label>
					</div>


				</fieldset>

			</div>
		</div>

	</div>

	<button type="button" data-target="field" data-show="fields" class="cancel">Cancelar</button>
	<button type="button" id="action-field" class="primary" data-action=""></button>


</form>