<form id="form-category">

	<div class="grid">
		<div class="row">
			<div class="span6">

				<fieldset>
					<legend>Categoría</legend>
					<label>Nombre de la Categoría</label>
					<div class="input-control text size4">
					    <input type="text" value="" name="category"/>
					    <button class="btn-clear"></button>
					</div>


				</fieldset>

			</div>
		</div>

	</div>

	<button type="button" data-target="category" data-show="categories" class="cancel">Cancelar</button>
	<button type="button" id="action-category" class="primary" data-action=""></button>


</form>