<form method="post" action="{{URL::to('login')}}" id="form-login">

	<label>Usuario</label>
	<div class="input-control text">
	    <input type="text" value="" name="login-username" placeholder="Ingrese su Usuario"/>
	    <button class="btn-clear"></button>
	</div>

	<label>Contraseña</label>
 	<div class="input-control password">
	    <input type="password" value="" name="login-password" placeholder="Ingrese su Contraseña"/>
	    <button class="btn-reveal"></button>
	</div>

	<button type="button" class="success" id="trigger-login">Ingresar</button>

</form>