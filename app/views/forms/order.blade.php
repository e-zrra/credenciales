<form id="form-order">

	<div class="grid">
		<div class="row">
			<div class="span6">
				<fieldset>
					
					<legend>Datos Generales</legend>
					@if(Auth::user()->hasRole('SUPERADMIN'))
						<label>Cliente:</label>
						<div class="input-control select">
						    <select name="client">
						       @foreach($clients as $client)
						       		<option value="{{$client->id}}">{{$client->name}}</option>
						       @endforeach
						    </select>
						</div>

					@else
						<input type="hidden" name="client" value="{{Auth::user()->client->id}}">

					@endif

					<label>Numero de Credenciales:</label>
					<div class="input-control text size2">
					    <input type="text" value="" name="noitems"/>
					    <button class="btn-clear"></button>
					</div>

					<label>Fecha Conveniente para Entrega:</label>
					<div class="input-control text size2" id="datepicker">
					    <input type="text" name="duedate">
					    <button class="btn-date"></button>
					</div>
					
				</fieldset>


			</div>

			<div class="span6">
				<fieldset>
					<legend>Notas</legend>
					<label>Notas:</label>
					<div class="input-control textarea">
					    <textarea name="notes"></textarea>
					</div>
				</fieldset>
			</div>
		</div>
	</div>

	<button type="button" id="cancel">Cancelar</button>
	<button type="button" id="action-order" class="primary" data-action=""></button>

</form>