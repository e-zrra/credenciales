<form id="form-client">

	<div class="grid">
		<div class="row">
			<div class="span6">
				<fieldset>
					
					<legend>Datos Generales</legend>
					<label>Nombre del Cliente:</label>
					<div class="input-control text">
					    <input type="text" value="" name="name"/>
					    <button class="btn-clear"></button>
					</div>

					<label>Dirección:</label>
					<div class="input-control text">
					    <input type="text" value="" name="address"/>
					    <button class="btn-clear"></button>
					</div>

					<label>Correo Electrónico:</label>
					<div class="input-control text size5">
					    <input type="text" value="" name="email"/>
					    <button class="btn-clear"></button>
					</div>

					<label>Teléfono:</label>
					<div class="input-control text size5">
					    <input type="text" value="" name="phone"/>
					    <button class="btn-clear"></button>
					</div>

					<label>Categoría:</label>
					<div class="input-control select size3">
					    <select name="category">
					        @foreach($categories as $category)
					        	<option value="{{$category->id}}">{{$category->description}}</option>
					        @endforeach
					    </select>
					</div>


				</fieldset>


			</div>

			<div class="span6">
				<fieldset>
					<legend>Datos de Acceso</legend>
					<label>Nombre de Usuario:</label>
					<div class="input-control text size4">
					    <input type="text" value="" name="username"/>
					    <button class="btn-clear"></button>
					</div>
					<br>
					<div class="input-control checkbox">
					    <label>
					        <input type="checkbox" name="send-to-email"/>
					        <span class="check"></span>
					        Enviar las datos de acceso por correo <br>
					         La Contraseña por defecto es <strong>password</strong>
					    </label>
					</div>

				</fieldset>

			</div>
		</div>
	</div>

	<button type="button" id="cancel">Cancelar</button>
	<button type="button" id="action-client" class="primary" data-action=""></button>

</form>