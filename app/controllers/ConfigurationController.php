<?php

class ConfigurationController extends BaseController {

	public function EmailAprovee()
	{
		$input = Input::get();

		DB::table('configuration')->where('id','=',1)->update(array('emailaprovee' => $input['aprovee']));

		$this->message 	= 'Actualizacion Exitosa!';
		$this->bg 		= '#60a917';
		$this->caption  = 'Exito!';
		

		return Response::json(
							array(
								'success'	=> true,
								'caption'	=> $this->caption,
								'message'	=> $this->message,
								'bg'		=> $this->bg,
								'fg'		=> 'white'
								)
							);	


	}

	public function AllowDesigner()
	{
		$input = Input::get();

		DB::table('configuration')->where('id','=',1)->update(array('allowdesigner' => $input['allow']));

		$this->message 	= 'Actualizacion Exitosa!';
		$this->bg 		= '#60a917';
		$this->caption  = 'Exito!';
		

		return Response::json(
							array(
								'success'	=> true,
								'caption'	=> $this->caption,
								'message'	=> $this->message,
								'bg'		=> $this->bg,
								'fg'		=> 'white'
								)
							);	


	}

	public function UpdateAdminEmail()
	{
		$input = Input::get();

		$rules = array(
						
						'adminemail'		=> 'required|email'
			);

		$messages = array(
						'adminemail.required' => 'Email requerido',
						'adminemail.email'	=> 'Formato de Email incorrecto'
			);

		$validator = Validator::make($input,$rules,$messages);

		if($validator->fails())
		{
			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
		}
		else
		{
			

			try
			{
				DB::table('configuration')->where('id','=',1)->update(array('adminemail' => $input['adminemail']));

				$this->message 	= 'El Email fue actualizado Exitosamente!';
				$this->bg 		= '#60a917';
				$this->caption  = 'Exito!';
				

				return Response::json(
									array(
										'success'	=> true,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white'
										)
									);	



			}
			catch(PDOexception $ex)
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno'.$ex,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}
		}

	}


}