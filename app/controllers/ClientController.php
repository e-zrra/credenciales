<?php

class ClientController extends BaseController {


	public function GetDesigns()
	{
		$record = Client::find(Input::get('client'));

		$designs = $record->designs()->paginate(1);

		$view   = View::make('pagination.designs')->with(array('records'=>$designs))->render();

		return Response::json(array('view' => $view));
		
	}
	
	public function Remove()
	{
		$record = Client::find(Input::get('id'));

		try
		{
			$record->delete();

			return Response::json(
								array(
									'success'	=> true,
									'caption'	=> 'Exito!',
									'message'	=> 'El Cliente fue Eliminado Exitosamente!',
									'bg'		=> '#60a917',
									'fg'		=> 'white'
									)
								);

		}
		catch(PDOexception $ex)
		{
			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

		}

	}


	

	public function GetByID()
	{
		$record = Client::with('user')->find(Input::get('id'));
		return Response::json(
								array(
										'record' 		=> $record
										)
								);

	}

	public function Get()
	{
		$search    = Input::get('search');
		$category  = Input::get('category');

		$query   = Client::query();

		if(Auth::user()->hasRole('SUPERADMIN'))
		{
			if($search)
			{
				$query->where('code','LIKE','%'.$search.'%')->orWhere('name','LIKE','%'.$search.'%');
			}

			if($category)
			{
				$query->where('category_id','=',$category);
			}
		}
		else
		{
			$query->where('id','=',Auth::user()->client->id);
		}

		$records = $query->get();

		$view    = View::make('pagination.clients')->with(array('records' => $records))->render();

		return Response::json(array('view' => $view));
	}

	public function Update()
	{
		$input = Input::get();

		$rules = array(
						'name' 		=> 'required',
						'address' 	=> 'required',
						'email'		=> 'required|email',
						'phone' 	=> 'required'

			);

		$messages = array(

						'name.required' 	=> 'Nombre del Cliente Requerido',
						'address.required' 	=> 'Dirección Requerida',
						'email.required' 	=> 'Dirección de Correo Electrónico Requerido',
						'phone.required' 	=> 'Teléfono Requerido',
						'email.email'		=> 'Dirección de Correo Inválida!' 

			);

		$validator = Validator::make($input,$rules,$messages);

		if($validator->fails())
		{

			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

		}
		else
		{

			$client = Client::find($input['id']);

			$client->name 		 = $input['name'];
			$client->address 	 = $input['address'];
			$client->email  	 = $input['email'];
			$client->phone  	 = $input['phone'];
			$client->category_id = $input['category'];


			try
			{
				$client->save();

				return Response::json(
								array(
									'success'	=> true,
									'caption'	=> 'Exito!',
									'message'	=> 'El Cliente se Actualizó Exitosamente!',
									'bg'		=> '#60a917',
									'fg'		=> 'white',
									'record'	=> Client::with('category')->where('id','=',$input['id'])->first()
									)
								);


			}
			catch(PDOexception $ex)
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}


		}


	}

	public function Add()
	{
		$input = Input::get();

		$rules = array(
						'name' 		=> 'required',
						'address' 	=> 'required',
						'email'		=> 'required|email',
						'phone' 	=> 'required',
						'username' 	=> 'required|unique:users,username'

			);

		$messages = array(

						'name.required' 	=> 'Nombre del Cliente Requerido',
						'address.required' 	=> 'Dirección Requerida',
						'email.required' 	=> 'Dirección de Correo Electrónico Requerido',
						'phone.required' 	=> 'Teléfono Requerido',
						'username.required' => 'Nombre de Usuario Requerido',
						'username.unique'  	=> 'Nombre de Usuario ya Existe',
						'email.email'		=> 'Dirección de Correo Inválida!' 

			);

		$validator = Validator::make($input,$rules,$messages);


		if($validator->fails())
		{

			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

		}
		else
		{

			$client = new Client;
			$user   = new User;

			$client->code        = Client::SetCode();
			$client->name 		 = $input['name'];
			$client->address 	 = $input['address'];
			$client->email  	 = $input['email'];
			$client->phone  	 = $input['phone'];
			$client->category_id = $input['category'];

			$user->username      = $input['username'];
			$user->password 	 = Hash::make('password');


			try
			{
				$client->save();
				$client->user()->save($user);

			/*	if($input['send-to-email'])
				{
					//pending task...	
				}
*/
				return Response::json(
								array(
									'success'	=> true,
									'caption'	=> 'Exito!',
									'message'	=> 'El Cliente se Registró Exitosamente!',
									'bg'		=> '#60a917',
									'fg'		=> 'white'
									)
								);
			}
			catch(PDOexception $ex)
			{

				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}	

		}

	}
}
