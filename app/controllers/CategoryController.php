<?php

class CategoryController extends BaseController {

	private $message = '';
	private $bg      = '';
	private $status  = null;
	private $caption = '';


	public function UpdateField()
	{
		$input = Input::get();

		$rules = array(
						'field' => 'required',
						'value'		=> 'required'
			);

		$messages = array(
						'value.required' => 'Nombre del Campo requerido'
			);

		$validator = Validator::make($input,$rules,$messages);

		if($validator->fails())
		{
			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
		}
		else
		{
			$field = Field::find($input['field']);

			try
			{
				$field->displayname = $input['value'];
				$field->save();

				$this->message 	= 'El Campo fue actualizado Exitosamente!';
				$this->bg 		= '#60a917';
				$this->caption  = 'Exito!';
				

				return Response::json(
									array(
										'success'	=> true,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white',
										'value'  	=> $input['value']
										)
									);	



			}
			catch(PDOexception $ex)
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno'.$ex,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}
		}

	}

	public function UpdateCategory()
	{
		$input = Input::get();

		$rules = array(
						'category' => 'required',
						'value'		=> 'required'
			);

		$messages = array(
						'value.required' => 'Nombre de la categoría requerido'
			);

		$validator = Validator::make($input,$rules,$messages);

		if($validator->fails())
		{
			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
		}
		else
		{
			$category = Category::find($input['category']);

			try
			{
				$category->description = $input['value'];
				$category->save();

				$this->message 	= 'El Campo fue actualizado Exitosamente!';
				$this->bg 		= '#60a917';
				$this->caption  = 'Exito!';
				

				return Response::json(
									array(
										'success'	=> true,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white',
										'value'  	=> $input['value']
										)
									);	



			}
			catch(PDOexception $ex)
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno'.$ex,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}
		}

	}

	public function GetFields()
	{

		$fields = DB::table('fields')->where('category_id','=',Input::get('category'))->get();

		$view = View::make('pagination.fields')->with(array('fields'=>$fields))->render();

		return Response::json(array('view'=>$view));


	}

	public function AddField()
	{
		$input = Input::get();

		$rules = array(
						'field' =>'required'

			);

		$messages = array(

						'field.required' => 'Nombre del Campo Requerido'
			);

		$validator = Validator::make($input,$rules,$messages);

		if($validator->fails())
		{
			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
		}
		else
		{	

			$category = Category::find($input['category']);

			$field  = new Field;

			$field->fieldname   = 'field'.Field::max('id');
			$field->displayname = $input['field'];
			$field->type        = $input['fieldtype'];


			if($field->type == 'text')
			{
				$field->datatarget = 'text-tools';
				
			}

			if($field->type == 'image')
			{
				$field->datatarget = 'image-tools';
				
			}

			if(isset($input['barcode']))
			{
				$field->generates = 'barcode';

				
			}

			if(isset($input['photo']))
			{
				$field->generates = 'photo';$field->datatarget = 'image-tools';
			}



			try
			{
				$category->fields()->save($field);

				Schema::table($category->table, function($table) use($field) 
				{

				    $table->string($field->fieldname)->nullable()->default(null);
				});

				$this->message 	= 'El Campo fue Registrado Exitosamente!';
				$this->bg 		= '#60a917';
				$this->caption  = 'Exito!';
				

				return Response::json(
									array(
										'success'	=> true,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white',
										'field'  	=> $field
										)
									);	



			}
			catch(PDOexception $ex)
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno'.$ex,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}



		}

	}


	public function Add()
	{
		$input = Input::get();

		$rules = array(
						'category' => 'required'
			);

		$messages = array(
						'category.required' => 'Categoría Requerida'
			);

		$validator = Validator::make($input,$rules,$messages);

		if($validator->fails())
		{

			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
		}
		else
		{

			$category = new Category;



			$category->table = 'table'.Category::max('id');
			$category->description = $input['category'];

			
			$this->message 	= 'La Categoría fue Registrada Exitosamente!';
			$this->bg 		= '#60a917';
			$this->caption  = 'Exito!';


			try
			{
				$category->save();
				Schema::create($category->table, function($table)
				{
				    $table->increments('id');
				    $table->integer('order_id');
				});

				return Response::json(
									array(
										'success'	=> true,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white',
										'category'  => $category
										)
									);	



			}
			catch(PDOexception $ex)
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}

		}


	}


}