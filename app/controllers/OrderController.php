<?php

class OrderController extends BaseController {

	private $message = '';
	private $bg      = '';
	private $status  = null;
	private $caption = '';

	private function SetCode()
	{

		$code = DB::table('orders')->max('id');
		$code++;

		switch ($code) {

			case ($code < 10 && $code > 0):
					$code = 'S000'.$code;
				break;
			case ($code < 100 && $code >= 10):
					$code = 'S00'.$code;
				break;
			case ($code < 1000 && $code >= 100):
				$code = 'S0'.$code;
				break;
			case ($code >= 1000):
				$code = 'S'.$code;
			break;
			
		}

		return $code;
	}

	public function RecordMarkAsCompleted()
	{

		$data = Input::get('records');

		try
		{
			DB::beginTransaction();

			foreach ($data['items'] as $key => $value) {
				
				DB::table($data['table'])->where('id','=',$value)->update(array('status_id' => 1));

			}

			DB::commit();

			$this->message 	= 'Actualización Exitosa!';
			$this->bg 		= '#60a917';
			$this->caption  = 'Exito!';

			return Response::json(
								array(
									'success'	=> true,
									'caption'	=> $this->caption,
									'message'	=> $this->message,
									'bg'		=> $this->bg,
									'fg'		=> 'white'
									)
								);	



		}
		catch(PDOexception $ex)
		{
			DB::rollback();
			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

		}



	}

	public function SetDesign()
	{
		$order = Order::find(Input::get('order'));
		$design = Input::get('design');


		try
		{

			$order->credential_id = $design;
			$order->save();

			$this->message 	= 'Se asignó correctamente el diseño!';
			$this->bg 		= '#60a917';
			$this->caption  = 'Exito!';

			return Response::json(
								array(
									'success'	=> true,
									'caption'	=> $this->caption,
									'message'	=> $this->message,
									'bg'		=> $this->bg,
									'fg'		=> 'white',
									'credential'=> $order->credential
									)
								);	

		}
		catch(PDOexception $ex)
		{
			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
		}

	}

	public function RemoveRecord()
	{
		$input = Input::get();


		if(isset($input['imagefield']))
		{
			$imagefield = $input['imagefield'];

			$order = Order::find($input['order']);

			if(File::exists(public_path().'/images/orders/o'.$order->id.'/o'.$order->id.'-'.$input['id']. '.png'))
			{
				File::delete(public_path().'/images/orders/o'.$order->id.'/o'.$order->id.'-'.$input['id']. '.png');
			}

		}

		try
		{
			$record = DB::table($input['table'])->where('id','=',$input['id'])->delete();

			$this->message 	= 'Se eliminó correctamente el registro!';
			$this->bg 		= '#60a917';
			$this->caption  = 'Exito!';

			return Response::json(
								array(
									'success'	=> true,
									'caption'	=> $this->caption,
									'message'	=> $this->message,
									'bg'		=> $this->bg,
									'fg'		=> 'white'
									)
								);	

		}
		catch(PDOexception $ex)
		{
			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

		}
	}


	public function UpdateRecord()
	{
		$input = Input::get();

		$id    		= $input['recordid'];
		$table 		= $input['tablename'];
		$orderid 	= $input['order'];

		if(isset($input['imagefield']))
		{
			$imagefield = $input['imagefield'];
			
			$img 		= $input[$imagefield];

			//$type = substr($img, 5, strpos($img, ';')-5);

			/*
			$img 		= $input[$imagefield];
			$img 		= str_replace('data:image/png;base64,', '', $img);
			$img 		= str_replace(' ', '+', $img);
			$data 		= base64_decode($img);
			$filename 	= 'images/orders/o'.$orderid.'/o'.$orderid.'-'.$id. '.png';
			$file 		= public_path() .'/'.$filename;
			*/

			$filename 	= 'images/orders/o'.$orderid.'/o'.$orderid.'-'.$id. '.jpg';
			$file 		= public_path() .'/'.$filename;

			$img = str_replace('data:image/jpeg;base64,', '', $img);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);

			file_put_contents($file, $data);

			$input[$imagefield] = $filename;


			//file_put_contents($file, $data);
			
		}

		unset($input['recordid']);
		unset($input['tablename']);
		unset($input['imagefield']);
		unset($input['order']);
	
		try
		{
			DB::table($table)
        		->where('id', $id)
        			->update($input);


				$this->message 	= 'Se Actulizó correctamente el registro!';
				$this->bg 		= '#60a917';
				$this->caption  = 'Exito!';

				return Response::json(
									array(
										'success'	=> true,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white'
										)
									);	

        }
        catch(PDOexception $ex)
        {
        	return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);



        }
			

	}


	public function GetRecords()
	{


    	$page       = Input::get('page');
    	$perpage    = Input::get('perpage');
    	$status     = Input::get('status');  
        $search     = Input::get('search');
        $searchfields = Input::get('searchfields');


        $order      = Input::get('order');
        $order  	= Order::find($order);
        
        $records    = DB::table($order->client->category->table)
        					->where('order_id','=',$order->id)
        					->where('status_id','=',$status)
        					->where(function($q) use($searchfields,$search){

        						if($searchfields){
        							foreach ($searchfields as $key) {
        							
        								$q->orWhere($key,'LIKE','%'.$search.'%');

        							}
        						}

        					})
        					
        					->paginate($perpage);

        $view       = View::make('pagination.paginator')->with(array('records'=>$records))->render();

        return Response::json(array('records'=>$records->getItems(),'view' => $view));
 	
	}

	

	public function UploadExcel()
	{
		$file = Input::file('file');
		$order = Input::get('order');
		//$columnstest = Input::get('columns');

		//echo var_dump($columnstest);

		$credential = Credential::find(Input::get('credential'));//->elements()->where('link_id','<>', 0)->where('data_type','=','text')-get();
		$links   	= $credential->elements()->select('link_id')->where('link_id','<>', 0)->where('data_type','=','text')->get();

		//$columnsTemp = Field::select('fieldname')->whereIn('id',$links->toArray())->get();

		//$columns = explode(',',Input::get('columns'));

		/*foreach ($columnsTemp as $column) {
			
			array_push($columns, $column->fieldname);

		}*/

		$ext 	= pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
		$name   = 'a'.uniqid().'.'.$ext;

		$file->move(public_path().'/tempexcels/',$name);

		//$myValueBinder = new MyValueBinder;

		//$result = Excel::selectSheetsByIndex(0)->load(public_path().'/tempexcels/'.$name, function($reader) {

			//$reader->noHeading();//important!!!
		//	$reader->toArray();
			//$reader->formatDates(false);


		///})->get();

		$reader = Excel::selectSheetsByIndex(0)->load(public_path().'/tempexcels/'.$name, function($reader) {
			$reader->noHeading();
			$reader->toArray();
		})->get();
		
		try
		{

		
			#$header_fields = $reader[0]; //fields of the head sheet

			$columns = array('order_id' => $order, 'status_id' => 0);
			
			#$data = array();

			for ($i=1; $i < count($reader); $i++) {

				$field = null;

				#$columns = array();

				for ($e=0; $e < count($reader[$i]); $e++) {

					if ($reader[0][$e] or $reader[0][$e] =! null or $reader[0][$e] > 0) {

						
						$field = Field::where('displayname', "{$reader[0][$e]}" )->first(); 

						
					
						if (!is_null($field)) {

							#$this->message .= $reader[$i][$e].'---';

							$array_fields =  array("$field->fieldname" => $reader[$i][$e]);

							$columns = array_merge($columns,$array_fields);
						}

					}

				}

				DB::table('table')->insert(
				    $columns
				);

			}

			//File::delete(public_path().'/tempexcels/'.$name);

			$this->message 	= 'Los Registros fueron ingresados Exitosamente!';
			$this->bg 		= '#60a917';
			$this->caption  = 'Exito!';

			//DB::commit();

			return Response::json(
				array(
					'success'	=> true,
					'caption'	=> $this->caption,
					'message'	=> $this->message,
					'bg'		=> $this->bg,
					'fg'		=> 'white'
					)
				);	

		}
		catch(PDOexception $ex) {

			throw new Exception("Erro:" .$ex);
			
			//DB::rollback();

			return Response::json(
				array(
					'success'	=> false,
					'caption'	=> 'Error...'.$ex,
					'message'	=> 'Error Interno',
					'bg'		=> '#e51400',
					'fg'		=> 'white'
					)
				);

		}

	}

	public function DownloadExcel()
	{

		$credential = Credential::find(Input::get('id'));

		$elements   = $credential->elements()->select('textcontent')->where('link_id','<>', 0)->where('data_type','=','text')->get();

        $array = array();

        foreach ($elements as $element) {
        	array_push($array, $element->textcontent);
        }

        Excel::create('Plantilla Excel', function($excel) use($elements,$array) {

		    $excel->sheet('Registros', function($sheet) use($elements,$array) {

		        $sheet->setOrientation('landscape');

		        $sheet->setColumnFormat(array(
				    'A' => '@','B'=>'@','C'=>'@','D'=>'@','E'=>'@','F'=>'@','G'=>'@','H'=>'@','I'=>'@','J'=>'@','K'=>'@','L'=>'@','M'=>'@','N'=>'@','O'=>'@','P'=>'@','Q'=>'@'
				    ,'R'=>'@','S'=>'@','T'=>'@','U'=>'@','V'=>'@','W'=>'@','X'=>'@','Y'=>'@','Z'=>'@'
				   
				));

		       // $sheet->fromArray($array,null,'A1',false,true);

		       	$sheet->loadView('partials.exceltemplate')
		        		->with(array('records' =>$elements));

		    });

		   

		})->download('xls');	
	}

	

	public function Approve()
	{


		$record = Order::find(Input::get('id'));
		
		if($record->status_id != 2)
		{
			$record->status_id = 2;// in proccess...
			$record->save();

			$this->status 	= $record->status;
			$this->message 	= 'La Solicitud fue Aprovada Exitosamente!';
			$this->bg 		= '#60a917';
			$this->caption  = 'Exito!';
			
			$config = DB::table('configuration')->where('id','=',1)->first();

			Mail::send('emails.aproveenotification', array('email' => $config->adminemail,'order'=>$record->code), function($message) use($config,$order)
            {
                $message->to($order->client->email, 'Sistema Credenciales')->from($config->adminemail)->subject('Solicitud Aprobads');
            });

		}
		else
		{
			$this->status 	= null;
			$this->message 	= 'La Solicitud ya fue Aprovada Anteriormente!';
			$this->bg 		= '#0050ef';
			$this->caption  = 'Info!';

		}
	
		return Response::json(
									array(
										'success'	=> true,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white',
										'status'	=> $this->status
										)
									);	


	}

	public function Cancel()
	{
		$record = Order::find(Input::get('id'));

		if(Auth::user()->hasRole('SUPERADMIN'))
		{

			try
			{
				if($record->status_id != 4)
				{
					$record->status_id = 4;
					$record->save();

					$this->status 	= $record->status;
					$this->message 	= 'La Solicitud fue Cancelada Exitosamente!';
					$this->bg 		= '#60a917';
					$this->caption  = 'Exito!';


				}
				else
				{
					$this->status 	= null;
					$this->message 	= 'La Solicitud ya fue Cancelada Anteriormente!';
					$this->bg 		= '#0050ef';
					$this->caption  = 'Info!';
				}

				return Response::json(
									array(
										'success'	=> true,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white',
										'status'	=> $this->status
										)
									);	

			}
			catch(PDOexception $ex)
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}


		}
		else
		{

			$this->status 	= null;
			$this->message 	= 'No tiene los privilegios para cancelar!';
			$this->bg 		= '#e51400';
			$this->caption  = 'Error!';


			return Response::json(
									array(
										'success'	=> false,
										'caption'	=> $this->caption,
										'message'	=> $this->message,
										'bg'		=> $this->bg,
										'fg'		=> 'white',
										'status'	=> $this->status
										)
									);	
		}


	}

	public function Remove()
	{
		$record = Order::find(Input::get('id'));

		try
		{
			$record->delete();

			return Response::json(
								array(
									'success'	=> true,
									'caption'	=> 'Exito!',
									'message'	=> 'La Solicitud fue Eliminada Exitosamente!',
									'bg'		=> '#60a917',
									'fg'		=> 'white'
									)
								);

		}
		catch(PDOexception $ex)
		{
			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

		}

	}


	public function GetByID()
	{
		$record = Order::with('status','client.category','credential')->find(Input::get('id'));
		//$variablefields = $record->client->category->fields()->select('id','displayname','type','generates','datatarget')->get();
		

		return Response::json(array('record' => $record));

	}

	public function Get()
	{
		$search    = Input::get('search');
		$status    = Input::get('status');

		$query   = Order::query();

		
		if($status)
		{
			$query->where('status_id','=',$status);
		}
		else
		{
			$query->where('status_id','=',2)->orWhere('status_id','=',3);
		}

		if($search)
		{
			$query->where('code','LIKE','%'.$search.'%');
		}


		if(!Auth::user()->hasRole('SUPERADMIN'))
		{

			$query->where('client_id','=',Auth::user()->client->id);
		}
		
		$records = $query->get();
		

		$view    = View::make('pagination.orders')->with(array('records' => $records))->render();

		return Response::json(array('view' => $view));

	}

	public function Update()
	{

		$input = Input::get();
		
		$rules = array(
						'noitems' 	=> 'required|numeric'
						

			);

		$messages = array(

						'noitems.required' 	=> 'Ingresar No. de Credenciales',
						'noitems.numeric'   => 'Ingresar un valor Entero para el Número de Credenciales'
						
			);

		$validator = Validator::make($input,$rules,$messages);

		if($validator->fails())
		{

			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

		}
		else
		{

			$order = Order::find($input['id']);

			$order->client_id = $input['client'];
			$order->noitems   = $input['noitems'];
			$order->duedate   = DateTime::createFromFormat('d-m-Y',Input::get('duedate'))->format('Y-m-d');
			$order->notes     = $input['notes'];

			try
			{
				$order->save();

				return Response::json(
								array(
									'success'	=> true,
									'caption'	=> 'Exito!',
									'message'	=> 'La Solicitud se Actualizó Exitosamente!',
									'bg'		=> '#60a917',
									'fg'		=> 'white',
									'record'	=> Order::with('client','status')->where('id','=',$input['id'])->first()
									)
								);


			}
			catch(PDOexception $ex)
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}


		}


	}

	public function Add()
	{
		$input = Input::get();

		$rules = array(
						'noitems' 	=> 'required|numeric'
						

			);

		$messages = array(

						'noitems.required' 	=> 'Ingresar No. de Credenciales',
						'noitems.numeric'   => 'Ingresar un valor Entero para el Número de Credenciales'
						
			);

		$validator = Validator::make($input,$rules,$messages);


		if($validator->fails())
		{

			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

		}
		else
		{

			$order  = new Order;
			
			$order->client_id = $input['client'];
			$order->code      = $this->SetCode();
			$order->status_id = 3; //pending... to approve
			$order->noitems   = $input['noitems'];
			$order->duedate   = DateTime::createFromFormat('d-m-Y',Input::get('duedate'))->format('Y-m-d');
			$order->notes     = $input['notes'];

			


			try
			{
				$order->save();

				$config = DB::table('configuration')->where('id','=',1)->first();

				Mail::send('emails.newordernotification', array('email' => $config->adminemail,'order'=>$order), function($message) use($config)
                {
                    $message->to($config->adminemail, 'Sistema Credenciales')->from($config->adminemail)->subject('Nueva Solicitud!');
                });
				
				
				File::makeDirectory(public_path().'/images/orders/o'.$order->id,0775,true);

				return Response::json(
								array(
									'success'	=> true,
									'caption'	=> 'Exito!',
									'message'	=> 'La Solicitud se Registró Exitosamente!',
									'bg'		=> '#60a917',
									'fg'		=> 'white'
									)
								);
			}
			catch(PDOexception $ex)
			{

				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno'.$ex,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);

			}	

		}

	}
}