<?php

class UserController extends BaseController {


	public function Login()
	{
		$input = Input::get();

		$rules = array(
						'login-username' => 'required',
						'login-password' => 'required'
			);

		$messages = array(
						'login-username.required' => 'Nombre de Usuario Requerido',
						'login-password.required' => 'Contraseña Requerida'

			);

		$validator = Validator::make($input,$rules,$messages);

		if($validator->fails())
		{

			
			$message = '';
			
			foreach ($validator->errors()->all() as $key => $value) {
				
				$message .= $value."<br>";
			}

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> $message,
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
		}
		else
		{

			if (Auth::attempt(array('username' => $input['login-username'], 'password' => $input['login-password']),true))
			{
			    return Response::json(array('success'=>true,'redirect'=>route('dashboard')));
			}
			else
			{
				return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Usuario y/o Contraseña Incorrectos',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
			}

		}


	}

	public function Logout()
	{
		Auth::logout();
        return Redirect::to('/');
	}

}

