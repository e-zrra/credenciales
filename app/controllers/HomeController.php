<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function ShowDashboard()
	{
		$config     = DB::table('configuration')->first();
		return View::make('sections.dashboard')->with(array('config' => $config));
	}

	public function ShowClients()
	{
		$categories = DB::table('categories')->get();
		$fields = Field::select('id','displayname','type','generates','datatarget')->get();
		$config     = DB::table('configuration')->first();

		return View::make('sections.clients')->with(array('categories'=>$categories,'fields'=>$fields,'config' =>$config));
	}

	public function ShowOrders()
	{
		$status 	= DB::table('orderstatus')->get();
		$clients 	= Client::select('name','id')->get();
		$config     = DB::table('configuration')->first();

		return View::make('sections.orders')->with(array('status'=>$status,'clients' => $clients,'config' => $config));
	}

	public function ShowConfiguration()
	{
		$categories = DB::table('categories')->get();
		$config     = DB::table('configuration')->first();

		return View::make('sections.configuration')->with(array('categories'=>$categories,'config'=>$config));
	}

}
