<?php

class CredentialController extends BaseController {

	private $message = '';
	private $bg      = '';
	private $status  = null;
	private $caption = '';

	private function SetInfo(&$element,$input,$i,$view)
	{
		$element->data_target 	= $input[$view]['elements'][$i]['datatarget'];
		$element->data_type   	= $input[$view]['elements'][$i]['datatype'];
		$element->style 	  	= $input[$view]['elements'][$i]['style'];
		$element->class 	  	= $input[$view]['elements'][$i]['class'];
		$element->link_id		= $input[$view]['elements'][$i]['link'];
		$element->view 			= $view;

		if($element->data_type == 'text')
		{
			$element->textcontent = $input[$view]['elements'][$i]['elementcontent'];
		}
		else
		{
			$element->imagecontent = $input[$view]['elements'][$i]['elementcontent'];
		}

	}

	public function GenerateCredentials()
	{

		$input = Input::get();

		$order = Order::find($input['order']);

		


	}

	public function GetByID()
	{
		$credential = Credential::with('elements')->find(Input::get('credential'));

		return Response::json(array('credential'=>$credential));
	}

	public function SaveDesign()
	{
		$input = Input::get();

		$credential;

		$client = Client::find($input['client']);

		if(isset($input['credential']))
		{
			$credential = Credential::find($input['credential']);
		}
		else
		{
			$credential = new Credential;
			$credential->client_id = $input['client'];
		}

		if(isset($input['anverso']['background'])){
			$credential->A_background = $input['anverso']['background'];
		}
		else
		{
			$credential->A_background = null;
		}

		if(isset($input['anverso']['backgroundstyle'])){
			$credential->A_style      = $input['anverso']['backgroundstyle'];
		}

		if(isset($input['reverso']['background'])){
			$credential->R_background = $input['reverso']['background'];
		}
		else
		{
			$credential->R_background = null;
		}

		if(isset($input['reverso']['backgroundstyle'])){
			$credential->R_style      = $input['reverso']['backgroundstyle'];
		}

		if (isset($input['width'])) {
			$credential->width = $input['width'];
		}

		if (isset($input['height'])) {
			$credential->height = $input['height'];
		}

		try
		{

			DB::beginTransaction();


			$credential->save();
			$array    = array();
			$active  = array(); 

			$views = array('anverso','reverso');

			foreach ($views as $view) {
				
				if(isset($input[$view]['elements']))
				{

					for($i = 0; $i < count($input[$view]['elements']) ; $i++) {
						
						$element;

						if($input[$view]['elements'][$i]['dataid'])
						{
							$element = Element::find($input[$view]['elements'][$i]['dataid']);
							$this->SetInfo($element,$input,$i,$view);
							$element->save();
						}
						else
						{
							$element = new Element;
							$this->SetInfo($element,$input,$i,$view);
							$credential->elements()->save($element);

							array_push($array, [$element->id,$input[$view]['elements'][$i]['ref']]);

						}

						array_push($active, $element->id);
			
					}
				}


			}

			if(count($active) > 0 )
			{
				
				$credential->elements()->whereNotIn('id',$active)->delete();
			}
			else
			{
				$credential->elements()->delete();
			}

			DB::commit();

			
			$this->message 	= 'Se han GUardado los Cambios Exitosamente!';
			$this->bg 		= '#60a917';
			$this->caption  = 'Exito!';
			
			$credentialid     = $credential->id;

			return Response::json(
										array(
											'success'	 => true,
											'caption'	 => $this->caption,
											'message'	 => $this->message,
											'bg'		 => $this->bg,
											'fg'		 => 'white',
											'status'	 => $this->status,
											'new'		 => $array,
											'credential' => $credentialid
											)
										);	

		}
		catch(PDOexception $ex)
		{

			DB::rollback();

			return Response::json(
								array(
									'success'	=> false,
									'caption'	=> 'Error...',
									'message'	=> 'Error Interno',
									'bg'		=> '#e51400',
									'fg'		=> 'white'
									)
								);
		}

		
		
	}

}