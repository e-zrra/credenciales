<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{

	return View::make('sections.index');
});

Route::post('login','UserController@Login');

Route::group(array('before' => 'auth'), function()
{
    Route::get('dashboard',array('as'=>'dashboard','uses'=>'HomeController@ShowDashboard'));


    Route::get('clients',array('as'=>'clients','uses'=>'HomeController@ShowClients'));
    Route::get('clients/get','ClientController@Get');
    Route::get('clients/getbyid','ClientController@GetByID');
    Route::post('clients/new','ClientController@Add');
    Route::post('clients/update','ClientController@Update');
    Route::post('clients/remove','ClientController@Remove');
    Route::post('clients/savedesign','CredentialController@SaveDesign');
    Route::get('clients/designs','ClientController@GetDesigns');
    Route::get('clients/designs/getbyid','CredentialController@GetByID');
   // Route::post('clients/designs/makedefault','CredentialController@MakeDefault');


    Route::get('orders',array('as'=>'orders','uses'=>'HomeController@ShowOrders'));
    Route::get('orders/get','OrderController@Get');
    Route::get('orders/getbyid','OrderController@GetByID');
    Route::post('orders/new','OrderController@Add');	
    Route::post('orders/update','OrderController@Update');
    Route::post('orders/approve','OrderController@Approve');
    Route::post('orders/remove','OrderController@Remove');
    Route::post('orders/cancel','OrderController@Cancel');
    Route::post('orders/setdesign','OrderController@SetDesign');


    Route::post('orders/uploadtempimage','OrderController@UploadTempImage');
    Route::post('orders/savedesign','CredentialController@SaveDesign');
    Route::get('orders/downloadexcel',array('as'=>'DownloadExcel','uses'=>'OrderController@DownloadExcel'));
    Route::post('orders/uploadexcel','OrderController@UploadExcel');
    Route::get('orders/getrecords','OrderController@GetRecords');
    Route::post('orders/record/update','OrderController@UpdateRecord');
    Route::post('orders/record/remove','OrderController@RemoveRecord');
    Route::get('orders/generatecredentials','CredentialController@GenerateCredentials');
    Route::post('orders/records/markascompleted','OrderController@RecordMarkAsCompleted');

	//Route::get('orders/getrecords', array('as' => 'api.users', 'uses' => 'AdminController@getUsersDataTable'));

    Route::get('configuration',array('as'=>'configuration','uses'=>'HomeController@ShowConfiguration'));
    Route::get('configuration/categoryfields','CategoryController@GetFields');
    Route::post('configuration/savecategory','CategoryController@Add');
    Route::post('configuration/updatecategory','CategoryController@UpdateCategory');
    Route::post('configuration/savefield','CategoryController@AddField');
    Route::post('configuration/updatefield','CategoryController@UpdateField');
    Route::post('configuration/updateadminemail','ConfigurationController@UpdateAdminEmail');
    Route::post('configuration/allowdesigner','ConfigurationController@AllowDesigner');
    Route::post('configuration/emailaprovee','ConfigurationController@EmailAprovee');


    Route::get('logout','UserController@Logout');
});
