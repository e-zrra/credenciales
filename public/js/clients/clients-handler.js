
var item;
var selectedclient;
var designeraction;

getClients(null);

/**
 * Event that showa new client registration form
 * button element
 */
$('#new').click(function(){

	$('#main').hide();
	$('#client').show(600);
	$('#form-client')[0].reset();
	$('#form-client').find('#action-client').attr('data-action','save').text('Registrar');
	$('#form-client').find('input[name=username]').removeAttr('disabled');
	$(this).hide();

});

/**
 * Event that cancel new client registration and 
 * closes the form and shows the main section
 * button element
 */
$('#cancel').click(function(){
	
	$('#client').hide();
	$('#main').show(600);
	$('#form-client').find('#action-client').attr('data-action','');
	$('#form-client').find('.span6').slice(1,2).show();
	$('#new').show();

});


/**
 * Event Saves or Update Client Record
 * button element
 */
$('#action-client').click(function(){

	var data 	= $('#form-client').serializeArray(); 
	var result;

	if($(this).attr('data-action') == 'save')
	{
		result 	= ajaxCall('post','clients/new',data);
		getClients();
	}
	else
	{
		data.push({'name':'id','value':item});
		result 	= ajaxCall('post','clients/update',data);
		refreshClientInfo(result);	
		
	}

	$.Notify({
			caption:  result.caption,
			content:  result.message,
			timeout: 5000,
			style: {background: result.bg,color:result.fg}

	});


	if(result.success == true)
	{

		$('#form-client')[0].reset();
		$('#client').hide();
		$('#main').show(600);
		$('#form-client').find('#action-client').attr('data-action','');
		$('#new').show();
		
	}
		
});


/**
 * Event that show information about Selected Client
 * list element
 */
$(document).on('click','.list',function(e){

	e.preventDefault();

	if(item != $(this).attr('data-id'))
	{
		item  = $(this).attr('data-id');
		$('#details').show();

		$(this).addClass('active');
		$('.list').not($(this)).removeClass('active');

		var data   = {id:$(this).attr('data-id')};
		var result = ajaxCall('get','clients/getbyid',data);

		selectedclient = result.record;
		setClientDetails();
		getClientDesigns();
		

	}

});


/**
 * Event that shows client information in the form
 * that can be updated
 * button element
 */
$('#client-edit').click(function(){

	$('#main').hide();
	$('#client').show(600);
	$('#form-client').find('#action-client').attr('data-action','update').text('Actualizar');
	$('#new').hide();

	var data 	= {id:item};
	var result 	= ajaxCall('get','clients/getbyid',data);

	setClientInfoIntoForm(result);

});

/**
 * Event that sends request to remove client
 * button element
 */
$('#client-remove').click(function(){

	if (confirm("Esta Seguro de Eliminar al cliente") == true) {
       	
       	var data = {id:item};

		var result = ajaxCall('post','clients/remove',data);

		if(result.success == true)
		{

			$('#details').hide();
			$('a.list.active').parent().remove();
		}

		$.Notify({
				caption:  result.caption,
				content:  result.message,
				timeout: 5000,
				style: {background: result.bg,color:result.fg}

		});

    } 

});


/**
 * Event that shows the client credentials desins
 * button element
 */
$('#client-designs').click(function(){

	$('#main').hide();
	$('#designs').show(600);
	$('#new').hide();

});

/**
 * Event that seach client by criteria 
 * buttton element
 */
$('.btn-search').click(function(){

	item = null;
	var data 	= getSearchParemeters();
	getClients(data);

});


/**
 * Event that shows clients with specific category
 * select element
 */
$('#category-search').change(function(){
	item = null;
	var data 	= getSearchParemeters();
	getClients(data);
});

/**
 * Event that shows credential designer
 * button element
 */
$(document).on('click','.showdesigner',function(){

 	$('#designs').hide();
 	$('#designer').show(600);

 	designeraction = $(this).attr('data-action');
 	

 	if(designeraction == 'save')
 	{
 		resetDesigner();
 	}
 	else
 	{
 		//selectedcredential = $(this).attr('data-id');
 		
 		if(selectedcredential != $(this).attr('data-id'))
 		{
 			selectedcredential = $(this).attr('data-id');
	 		resetDesigner();
	 		rebuildDesign();
 		}
 	}

 });

/**
 * Event that returns to main view
 * a element
 */
$('.backtomain').click(function(e){
 	
 	e.preventDefault();

 	$('#designs').hide();
 	$('#main').show(600);
 	
});

/**
 * Event that returns to designs view
 * a element
 */
$('#backtodesigns').click(function(e){

	e.preventDefault();

	$('#designer').hide();
 	$('#designs').show(600);
 	

});

var queryStringToJSON = function (url) {
    if (url === '')
        return '';
    var pairs = (url || location.search).slice(1).split('&');
    var result = {};
    for (var idx in pairs) {
        var pair = pairs[idx].split('=');
        if (!!pair[0])
            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
    }
    return result;
}

$(document).on('click', 'a.paglink', function (e) {
	
	e.preventDefault();

	if(!$(this).parent().hasClass('disabled')){

		

		var string = $(this).attr('href').split('designs')[1];

		var json = queryStringToJSON(string);

		selectedpage = $(this).text();

		json.client = selectedclient.id;
		var result = ajaxCall('get','clients/designs',json);

		$('#designscontent').html(result.view);
		
	}
   
});


/**
 * Get values from search and category
 */
function getSearchParemeters()
{
	var data = {
				category: 	$('#category-search').val(),
				search: 	$('#text-search').val()
				};

	return data;
}

function refreshClientInfo(data)
{

	var a = $('.active');

	a.find('.list-title').text(data.record.name);
	a.find('.list-subtitle').text(data.record.address);
	a.find('.list-remark').text(data.record.email);
	a.find('.list-subtitle').append('<span class="place-right">' + data.record.category.description + '</span>')
	

	$('#client-name').text('Nombre: ' + data.record.name);
	$('#client-address').text('Dirección: ' + data.record.address);
	$('#client-email').text('Correo Electrónico: ' + data.record.email);
	$('#client-phone').text('Teléfono: ' + data.record.phone);	
}

function setClientInfoIntoForm()
{
	var form = $('#form-client');

	form.find('input[name=name]').val(selectedclient.name);
	form.find('input[name=address]').val(selectedclient.address);
	form.find('input[name=email]').val(selectedclient.email);
	form.find('input[name=phone]').val(selectedclient.phone);
	form.find('select[name=category]').val(selectedclient.category_id);

	form.find('.span6').slice(1,2).hide();
	form.find('input[name=username]').attr('disabled','disabled');
}

function setClientDetails()
{
	$('#client-code').text('No Cliente: '+ selectedclient.code);
	$('#client-name').text('Nombre: ' + selectedclient.name);
	$('#client-address').text('Dirección: ' + selectedclient.address);
	$('#client-email').text('Correo Electrónico: ' + selectedclient.email);
	$('#client-phone').text('Teléfono: ' + selectedclient.phone);
	$('#client-dateadded').text('Fecha de Registro: ' + selectedclient.created_at);
	$('#client-username').text('Nombre de Usuario: ' + selectedclient.user.username);
}

function getClients(data)
{

	result = ajaxCall('get','clients/get',data);
	$('#clients').html('').html(result.view);
	$('#details').hide();
}

function getClientDesigns()
{

	$('#designs').find('h2#client-name').text('Cliente: ' + selectedclient.name);

	var data = {client: selectedclient.id};
	var result = ajaxCall('get','clients/designs',data);

	$('#designscontent').html(result.view);

}

function ajaxCall(type,url,data)
{
	var result;

	$.ajax({
			type: type,
			url: url,
			data: data,
			async:false
	})
  	.done(function( msg ) {
    	
  		result = msg;

  	});

  	return result;

}

