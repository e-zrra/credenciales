$(document).ready(function(){

	$(document).ready(function(){


			var item;
			var record;
			var selectedcredential = null;
			var selecteddesign = 1;

			var element;
			var containment = $('#anversodesigner');
			var elementref = 0;
			var recordselected;
			var variablefields;
			var headers;
			var tr;
			var arraytd = [];
			var dialog;
			var photobooth;
			var photoboothon = 0;

			
			var designeraction;
 

			containment.addClass('selected');

			$('#anversodesigner > img').css('width', parseFloat(containment.css('width')) - 2 );
			$('#anversodesigner > img').css('height',parseFloat(containment.css('height')) - 3); 
			$('#reversodesigner > img').css('width', parseFloat(containment.css('width')) - 2);
			$('#reversodesigner > img').css('height',parseFloat(containment.css('height')) - 3);

			$('#anversopreview > img').css('width', parseFloat(containment.css('width')) - 2 );
			$('#anversopreview > img').css('height',parseFloat(containment.css('height')) - 3); 
			$('#reversopreview > img').css('width', parseFloat(containment.css('width')) - 2);
			$('#reversopreview > img').css('height',parseFloat(containment.css('height')) - 3);
			



			$('.picker').colpick({	
				layout:'hex',
				submit:0,
				onChange: function(hsb,hex,rgb,el,bySetColor){


					$('#' + $(el).attr('data-target')).css('background-color','#' + hex);

					if($('.selected').hasClass('credentialview'))
					{
						$('.selected').css('background-color','#' + hex)
					}
					else{
						$('.selected').css('color','#' + hex);
					}


					
				}
			});
			
		
			$('#savedesign').click(function(){

				var width = $('.input-credencial-width').val(),
				height = $('.input-credencial-height').val();

				//console.log(width + height)

				var anverso = getElements('anverso');
				var reverso = getElements('reverso');

				var data = {};

				if(selectedcredential)
				{
					data = {client:record.id,credential:selectedcredential.id,anverso:anverso,reverso:reverso};
				}
				else
				{
					data = {client:record.id,anverso:anverso,reverso:reverso};
				}
				
				//console.log(data);

				var result = ajaxCall('post','clients/savedesign',data);

				if(result.success == true)
				{

					for (var i = 0; i < result.new.length; i++) {
					
						$('span#' + result.new[i][1]).attr('data-id',result.new[i][0]);

					};

					record = result.record;

					selectedcredential = result.design;

					resetPreview();
					updatePreview();
					

				}

				$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

					});

			});

			$('.removeimage').click(function(){

				

				containment.find('img#background').removeAttr('src');
				
				var input = null;

				if(containment.attr('id') == 'anversodesigner')
				{
					input = $('#anversoimage');
					input.replaceWith(input.val('').clone(true));
					$('#view-background-preview').removeAttr('src');
					
				}
				else
				{
					input = $('#reversoimage');
					input.replaceWith(input.val('').clone(true));
					$('#view-background-preview').removeAttr('src');
					
				}


			});

			
			$('.viewimage').change(function(){

				PreviewImage($(this).attr('id'),'view-background-preview',$(this).attr('data-type'));
			});


			$('button.elementimage').click(function(){

				$('#elementimage').trigger('click');

			});

		

			$('button.image').click(function(){
				
				if(containment.attr('id') == 'anversodesigner'){

					$('#anversoimage').trigger('click');
				}

				if(containment.attr('id') == 'reversodesigner')
				{
					$('#reversoimage').trigger('click');
				}



			});



			$(document).on('click','.credentialview > span.element',function(){

				$('.selected').removeClass('selected');
				$(this).addClass('selected');


				var target = $(this).attr('data-target');

				var elements = $(this).attr('data-target').split(",");
				$('#options > div').hide();
				
				$.each(elements,function(index,value){
					
					$('#' + value).show();
				});

				setElementToolProperties(target);


			});


			$('#elementremove').click(function(){

				if(!$('.selected').hasClass('credentialview'))
				{
					$('.selected').remove();
				}
				
			});


			$(document).on('click','.option',function(){


				$('.selected').removeClass('selected');

				if($(this).attr('data-title'))
				{
					$('#title-designer-view').text($(this).attr('data-title'));
				}

				if($(this).attr('data-containment'))
				{
					
					if(containment.attr('id') != $(this).attr('data-containment') )
					{
						containment.hide();
						$('#' + $(this).attr('data-containment')).show(400);
					}

					containment = $('#' + $(this).attr('data-containment'));
					containment.addClass('selected');

					setElementToolProperties($(this).attr('data-target'));

				}
				else
				{
					setElementToCanvas($(this));
				}
				
				var elements = $(this).attr('data-target').split(",");
				$('#options > div').hide();
				
				$.each(elements,function(index,value){
					
					$('#' + value).show();
				});

			});

			/*$(document).on('click','a.list',function(e){

				e.preventDefault();

				if(item != $(this).attr('data-id'))
				{
					item  = $(this).attr('data-id');
					$('#details').show();
					$('#credential').hide();

					$(this).addClass('active');
					$('.list').not($(this)).removeClass('active');

					var data = {id:$(this).attr('data-id')};
					var result = ajaxCall('get','orders/getbyid',data);

					setOrderDetails(result);

					record = result.record;

					setVariableFields(result.variablefields);

					resetPreview();
					updatePreview();
					resetDesigner();
					updateDesigner();
					
					$('#view-background-preview').removeAttr('src');
					$('.picker #viewcolor').css('background-color','#ffffff');
					$('#element-preview').removeAttr('src');
					
					$('#anversopreview').show();
					$('#reversopreview').hide();

					$('#anversodesigner').show();
					$('#reversodesigner').hide();
					
				}

			});*/

			$('.showpreview').click(function(){

				var target = $(this).attr('data-target');
				var hide = $(this).attr('data-hide');

				$('#' + target).show(400);
				$('#' + hide).hide();



			});



			$('.font-style').click(function(){

				if($('.selected').css($(this).attr('data-css')) == $(this).attr('data-value'))
				{	
					$(this).removeClass('inverse');
					$('.selected').css($(this).attr('data-css'),'');

				}
				else
				{
					$('.selected').css($(this).attr('data-css'),$(this).attr('data-value'));
					$(this).addClass('inverse');
				}
			})

			$('#fontsize').change(function(){


				$('.selected').css('font-size',$(this).val() + 'px');

			});

			$('#fontfamily').change(function(){

				$('.selected').css('font-family',$(this).val());


			});

			$('input[name=text]').keyup(function(){
				
				$('.selected').text($(this).val())

				if($(this).val() == '')
				{
					$('.selected').css('border','1px gray solid');
				}
				else
				{
					$('.selected').css('border','');	
				}

			});

			getClients(null);


			$('.backtodesigns').click(function(){

				$('#designer').hide();
				$('#designs').show(600);


			});

			$('.viewdesigner').click(function(){


				designeraction = $(this).attr('data-action');

				if(designeraction == 'save')
				{
					selectedcredential = null;
				}
			
				$('#designs').hide();
				$('#designer').show(600);

			});

			$('#client-designs').click(function(){


				$('#main').hide();
				$('#new').hide();
				$('#designs').show(600);



			});

			$('#backtomain').click(function(){


				$('#designs').hide();
				$('#main').show(600);
				$('#new').show();
				

			});

			$('#new').click(function(){

				$('#main').hide();
				$('#client').show(600);
				$('#form-client')[0].reset();
				$('#form-client').find('#action-client').attr('data-action','save').text('Registrar');
				$('#form-client').find('input[name=username]').removeAttr('disabled');
				$(this).hide();

			});

			$('#cancel').click(function(){
				

				$('#client').hide();
				$('#main').show(600);
				$('#form-client').find('#action-client').attr('data-action','');
				$('#form-client').find('.span6').slice(1,2).show();
				$('#new').show();


			});

			$('#action-client').click(function(){

				var data 	= $('#form-client').serializeArray(); 
				var result;

				if($(this).attr('data-action') == 'save')
				{
					result 	= ajaxCall('post','clients/new',data);
				}
				else
				{
					data.push({'name':'id','value':item});
					result 	= ajaxCall('post','clients/update',data);
					refreshClientInfo(result);	
					
				}

				$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

				});


				if(result.success == true)
				{

					$('#form-client')[0].reset();
					$('#client').hide();
					$('#main').show(600);
					$('#form-client').find('#action-client').attr('data-action','');
					$('#new').show();
					
				}
					
			});

			$(document).on('click','.list',function(e){

				e.preventDefault();

				if(item != $(this).attr('data-id'))
				{
					item  = $(this).attr('data-id');
					$('#details').show();

					$(this).addClass('active');
					$('.list').not($(this)).removeClass('active');

					var data = {id:$(this).attr('data-id')};
					var result = ajaxCall('get','clients/getbyid',data);

					record = result.record;

					if(result.designs == 0)
					{
						$('#designscontainer').html('<div class="notice marker-on-top">El Cliente no tiene Diseños de Credencial</div>');
					}
					else
					{
						showDesigns();
					}

					setClientDetails(result);

					setVariableFields(result.variablefields);

				}

			});

			$('#client-edit').click(function(){

				$('#main').hide();
				$('#client').show(600);
				$('#form-client').find('#action-client').attr('data-action','update').text('Actualizar');
				$('#new').hide();

				var data 	= {id:item};
				var result 	= ajaxCall('get','clients/getbyid',data);

				setClientInfoIntoForm(result);

			});

			$('#client-remove').click(function(){

				var data = {id:item};

				var result = ajaxCall('post','clients/remove',data);

				if(result.success == true)
				{

					$('#details').hide();
					$('a.list.active').parent().remove();
				}

				$.Notify({
						caption:  result.caption,
						content:  result.message,
						timeout: 5000,
						style: {background: result.bg,color:result.fg}

				});

			});

			$('.btn-search').click(function(){

				item = null;
				var data 	= getSearchParemeters();
				getClients(data);
			
			});

			$('#category-search').change(function(){
				item = null;
				var data 	= getSearchParemeters();
				getClients(data);
			});

			function hasImgContent(variable)
			{
				if(variable) 
				{ 
					record.designs[i].A_background;
				} 
			}

			function showDesigns()
			{
				html = '';

				for (var i = 0; i < record.designs.length; i++) {
					
					html += '<button data-action="edit" class="info viewdesigner" data-hint="Diseño|Editar Diseño" data-hint-position="top"><i class="icon-pencil"></i></button>';

					html += '<div class="row">'
					html += '<div class="span6">'

					html +=	'<div id="anversopreview' + record.designs[i].id +'" style="'+ record.designs[i].A_style +'display:block;"><img id="background" "' +  ( record.designs[i].A_background ? 'src="' + record.designs[i].A_background + '"' : 'src="#"' ) +  '" style="position:absolute;" />'

					for (var j = 0; j < record.designs.elements.length; j++) {
					
						switch(record.designs.elements[i].data_type)
						{
							case 'text':
								html += '<span style="' + record.designs.elements[i].style + '">' +  record.designs.elements[i].textcontent + '</span>';
							break;
							case 'image':
								html += '<span style="' + record.designs.elements[i].style + '"><img style="width:100%;height:100%;"' +  ( record.designs[i].imagecontent ? 'src="' + record.designs[i].A_imagecontent + '"' : 'src="#"' ) + '</span>';
							break;
							case 'barcode':
								$('#'+record.credential.elements[i].view+'preview').append('<span style="' + record.credential.elements[i].style +'"><img style="width:100%;height:100%;" src="' + record.credential.elements[i].imagecontent +'" /></span>');
							break;
						}	


					};

					html += '</div>'
						
					html +=	'</div>'

					html +=	'<div class="span6">'
					html +=	'<div id="reversopreview'+ record.designs[i].id +'" style="'+ record.designs[i].R_style +'display:block;" /><img id="background" "' +  ( record.designs[i].A_background ? 'src="' + record.designs[i].R_background + '"' : 'src="#"' ) +  '" style="position:absolute;" /></div>'
					html +=	'</div>'
					html += '</div><br>'


				};

				$('#designscontainer').html(html);
			}
			
	
			function setVariableFields(data)
			{
				var html = '';
				$.each(data,function(index,value){

					html += '<button style="margin:2px;" data-text="' + value.displayname +'" data-target="' + value.datatarget+ '" data-type="' + value.type +'" data-id="' + value.id +'" class="primary option">' + value.displayname +'</button><br>';

				});

				$('#variablefields').html(html);
			}


			function getSearchParemeters()
			{
				var data = {category:$('#category-search').val(),search:$('#text-search').val()}
				return data;
			}

			function refreshClientInfo(data)
			{

				var a = $('.active');

				a.find('.list-title').text(data.record.name);
				a.find('.list-subtitle').text(data.record.address);
				a.find('.list-remark').text(data.record.email);
				a.find('.list-subtitle').append('<span class="place-right">' + data.record.category.description + '</span>')
				

				$('#client-name').text('Nombre: ' + data.record.name);
				$('#client-address').text('Dirección: ' + data.record.address);
				$('#client-email').text('Correo Electrónico: ' + data.record.email);
				$('#client-phone').text('Teléfono: ' + data.record.phone);

				
			}


			function setClientInfoIntoForm(data)
			{
				var form = $('#form-client');

				form.find('input[name=name]').val(data.record.name);
				form.find('input[name=address]').val(data.record.address);
				form.find('input[name=email]').val(data.record.email);
				form.find('input[name=phone]').val(data.record.phone);
				form.find('select[name=category]').val(data.record.category_id);

				form.find('.span6').slice(1,2).hide();
				form.find('input[name=username]').attr('disabled','disabled');


			}

			function setClientDetails(data)
			{
				$('#client-code').text('No Cliente: '+ data.record.code);
				$('#client-name').text('Nombre: ' + data.record.name);
				$('#client-address').text('Dirección: ' + data.record.address);
				$('#client-email').text('Correo Electrónico: ' + data.record.email);
				$('#client-phone').text('Teléfono: ' + data.record.phone);
				$('#client-dateadded').text('Fecha de Registro: ' + data.record.created_at);
				$('#client-username').text('Nombre de Usuario: ' + data.record.user.username);
			}


			function getClients(data)
			{

				result = ajaxCall('get','clients/get',data);
				$('#clients').html('').html(result.view);
				$('#details').hide();
			}

			function resetPreview()
			{
				$('#anversopreview').css('background-color','#ffffff');
				$('#anversopreview').find('#background').removeAttr('src');
				$('#anversopreview').find('span').remove();
				

				$('#reversopreview').css('background-color','#ffffff');				
				$('#reversopreview').find('#background').removeAttr('src');
				$('#reversopreview').find('span').remove();

			}

			function updatePreview()
			{

				$('#anversopreview').attr('style',selectedcredential.A_style);
			
				if(selectedcredential.A_background){
					$('#anversopreview').find('#background').attr('src',selectedcredential.A_background);
				}

				$('#reversopreview').attr('style',selectedcredential.R_style);
			
				if(selectedcredential.R_background){
					$('#reversopreview').find('#background').attr('src',selectedcredential.R_background);
				}

				for (var i = 0; i < selectedcredential.elements.length; i++) {
					
					switch(selectedcredential.elements[i].data_type)
					{
						case 'text':
							$('#'+selectedcredential.elements[i].view +'preview').append('<span style="' + selectedcredential.elements[i].style + '">' +  selectedcredential.elements[i].textcontent + '</span>');
						break;
						case 'image':
							$('#'+selectedcredential.elements[i].view+'preview').append('<span style="' + selectedcredential.elements[i].style + '"><img style="width:100%;height:100%;" src="' + selectedcredential.elements[i].imagecontent +'" /></span>');
						break;
						case 'barcode':
							$('#'+selectedcredential.elements[i].view+'preview').append('<span style="' + selectedcredential.elements[i].style +'"><img style="width:100%;height:100%;" src="' + selectedcredential.elements[i].imagecontent +'" /></span>');
						break;
					}	


				};


			}

			function resetDesigner()
			{
				$('#anversodesigner').css('background-color','#ffffff');
				$('#anversodesigner').find('#background').removeAttr('src');
				$('#anversodesigner').find('span').remove();
				

				$('#reversodesigner').css('background-color','#ffffff');				
				$('#reversodesigner').find('#background').removeAttr('src');
				$('#reversodesigner').find('span').remove();

			}

			function updateDesigner()
			{

				$('#anversodesigner').attr('style',record.credential.A_style);
				
				//$('#anversodesigner').width(record.credential.width);
				//$('#anversodesigner').height(record.credential.height);
			
				if(record.credential.A_background){
					$('#anversodesigner').find('#background').attr('src',record.credential.A_background);
				}

				$('#reversodesigner').attr('style',record.credential.R_style);
			
				if(record.credential.R_background){
					$('#reversodesigner').find('#background').attr('src',record.credential.R_background);
				}

				for (var i = 0; i < record.credential.elements.length; i++) {

					$('.selected').removeClass('selected');
					
					switch(record.credential.elements[i].data_type)
					{
						case 'text':
							
							$('#'+record.credential.elements[i].view +'designer').append('<span data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'" style="' + record.credential.elements[i].style + '" class="element selected">' +  record.credential.elements[i].textcontent + '</span>');
							makeElementDraggable();

						break;
						case 'image':
							$('#'+record.credential.elements[i].view+'designer').append('<span data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'"  style="' + record.credential.elements[i].style + '" class="element selected"><img style="width:100%;height:100%;" src="' + record.credential.elements[i].imagecontent +'" /></span>');
							makeElementDraggable();
							makeElementRezisable();
						break;
						case 'barcode':
							$('#'+record.credential.elements[i].view+'designer').append('<span data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'"  style="' + record.credential.elements[i].style +'" class="element selected"><img style="width:100%;height:100%;" src="' + record.credential.elements[i].imagecontent +'" /></span>');
							makeElementDraggable();
							makeElementRezisable();
						break;
					}	


				};


			}

			function getElements(view)
			{
				var data = { 
								background: 		$('#'+view+'designer').find('#background').attr('src'),
								backgroundstyle: 	$('#'+view+'designer').attr('style'),
								elements: []
							};

				var i = 0;
				$.each($('#'+view+'designer').find('span'),function(){

					var content = '';

					//console.log(view)
					if($(this).attr('data-type') == 'text') { content = $(this).text(); } else { content = $(this).find('img').attr('src'); }

					data.elements[i] = {
						ref:                $(this).attr('id'),
						dataid: 			$(this).attr('data-id'),
						style: 				$(this).attr('style'),
						datatarget: 		$(this).attr('data-target'),
						datatype:   		$(this).attr('data-type'),
						class: 				'element',
						elementcontent: 	content,
						link: 				$(this).attr('data-link')

					}

					i++;

				});

				return data;

			}

			

			

			function PreviewImage(inputfile,imgPreview,type) {
		
	        	var oFReader = new FileReader();
	        	oFReader.readAsDataURL(document.getElementById(inputfile).files[0]);

	        	oFReader.onload = function (oFREvent) {

	        		
	            	document.getElementById(imgPreview).src = oFREvent.target.result;

	            	if(type == 'view'){
	            		containment.find('img#background').attr('src',oFREvent.target.result)
	            	}else{

	            		$('.selected').find('img').attr('src',oFREvent.target.result)
	            		$('.selected').find('img').attr('data-uri',true);
	            		
	            	}
	         	 		
	        	};

	    	};

			function setElementToCanvas(option)
			{

				switch(option.attr('data-type'))
				{
					case 'text':

						Content = 'Texto';
						
						if(option.attr('data-text'))
						{
							Content = option.attr('data-text');
							$('input[name=text]').attr('disabled','disabled');
							
						}
						else
						{
							$('input[name=text]').removeAttr('disabled');
						}


						element = '<span id="r_' + (elementref++) +'"  data-id="" data-link="' + option.attr('data-id') + '" data-target="texttools" style="color:#000000;font-family:Arial;font-weight:normal;text-decoration:none;font-style:normal;font-size:10px;position:absolute;cursor:move;" class="element selected" data-type="' + option.attr('data-type') + '">' + Content+ '</span>';
						containment.append(element);
						setElementToolProperties('texttools');
						makeElementDraggable();

					break;

					case 'image':

						element = '<span id="r_' + (elementref++) +'" data-id="" data-link="' + option.attr('data-id') + '" data-target="imagetools" style="position:absolute;cursor:move;width:50px;height:50px;" class="element selected" data-type="' + option.attr('data-type') + '"></span>';
						containment.append(element);
						
						if(option.attr('data-text'))
						{
							
							$('.selected').append('<img data-uri="false" style="width:100%;height:100%;" src="../public/img/user_icon.png" />');
							$('.elementimage').hide();
						}
						else
						{
							$('.selected').append('<img data-uri="false" style="width:100%;height:100%;" src="../public/img/image.png" />');
							$('.elementimage').show();
						}

						
						setElementToolProperties('imagetools');
						makeElementDraggable();
						makeElementRezisable();


					break;

					case 'barcode':
						element = '<span id="r_' + (elementref++) +'" data-id="" data-link="' + option.attr('data-id') + '" data-target="barcodetools" style="position:absolute;cursor:move;width:100px;height:50px;" class="element selected" data-type="' + option.attr('data-type') + '"></span>';
						containment.append(element);

						$('.selected').append('<img data-uri="false" style="width:100%;height:100%;" src="../public/img/barcode.jpg" />');
						
						makeElementDraggable();
						makeElementRezisable();


					break;


				}
				
				
			}

			function makeElementDraggable()
			{
				

				$('.selected').draggable({
					//containment: container,
					scroll: false,
					drag: function(){

					}
				});

			}

			function makeElementRezisable()
			{

				

				$('.selected').resizable({ 
					//containment: container,
					resize:function(){


					}
				});
			}

			function setElementToolProperties(target)
			{
				
				var selected = $('.selected');

				switch(target)
				{
					case 'texttools':


						if(selected.attr('data-link') == 'undefined' || selected.attr('data-link') == '0')
						{
							$('#texttools').find('input[name=text]').removeAttr('disabled');
						}
						else
						{
							$('#texttools').find('input[name=text]').attr('disabled','disabled');
						}

						$('#texttools').find('input[name=text]').val(selected.text());
						$('#texttools').find('#textcolor').css('background-color',selected.css('color'));
						$('#texttools').find('#fontsize').val(selected.css('font-size').replace('px',''));
						$('#texttools').find('#fontfamily').val(selected.css('font-family').replace(/'/g, ''));
						$('#texttools').find('.font-style').removeClass('inverse');

						if(selected.css('font-weight') == 'bold'){
							$('.font-style').slice(0,1).addClass('inverse');
						}

						if(selected.css('font-style') != 'normal')
						{
							$('.font-style').slice(1,2).addClass('inverse');
						}

						if(selected.css('text-decoration') != 'none')
						{
							$('.font-style').slice(2,3).addClass('inverse');
						}



					break;

					case 'imagetools':

						
						$('#imagetools > div > div > img').attr('src', selected.find('img').attr('src'));

						if(selected.attr('data-link') == 'undefined')
						{
							$('#imagetools').find('.elementimage').show();
						}
						else
						{
							$('#imagetools').find('.elementimage').hide();
						}

					break;

					case 'viewtools':
						$('#viewtools').find('#viewcolor').css('background-color',containment.css('background-color'));

							if(containment.find('img').attr('src'))
							{
								$('#view-background-preview').attr('src', containment.find('img').attr('src') );
							}
							else
							{
								$('#view-background-preview').removeAttr('src');
							}
							
					break;
				}

			}

			function getSearchParemeters()
			{
				var data = {status:$('#status-search').val(),search:$('#text-search').val()}
				return data;
			}



			function ajaxCall(type,url,data)
			{
				var result;

				$.ajax({
						type: type,
						url: url,
						data: data,
						async:false
				})
			  	.done(function( msg ) {
			    	
			  		result = msg;

			  	});

			  	return result;

			}

			function ajaxCall2(type,url,data,processData,contentType)
			{
				var result;

				$.ajax({
						type: type,
						url:  url,
						data: data,
						cache: false,
	    				contentType: contentType,
	    				processData: processData,
						async:false
				})
			  	.done(function( msg ) {
			    	
			  		result = msg;

			  	});

			  		return result;

			}
		});


});