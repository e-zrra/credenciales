
var selectedview;
var selectedoption;
var elementref = 1;
var selectedcredential;
var selectedpage;




/**
 * Event that shows element tools
 * button element
 */
$('.option').click(function(){
	
	var option = $(this);

	
		$('#tools > div').hide();
		$('#' + option.attr('data-target')).show();
	

	if(option.attr('data-target') == 'view-tools')
	{
		selectedview = $('#' + option.attr('data-selectedview'));

		//selectedview.css('border-color','#4390df');
		$('#canvas > div').not(selectedview).css('border-color','gray');

		if(selectedview.find('img#background').attr('src'))
		{
			$('#view-image-preview').attr('src',selectedview.find('img#background').attr('src'));
		}
		else
		{
			$('#view-image-preview').removeAttr('src');
		}

		$('#view-color').css('background-color',selectedview.css('background-color'));


	}

	addElementToCanvas(option);
	
});

/**
 * Event that change element text on typing
 * input text element
 */
$('#element-text').keyup(function(){
				
	$('span.selected').text($(this).val())

	if($(this).val() == '')
	{
		$('span.selected').css('border','1px gray solid');
	}
	else
	{
		$('span.selected').css('border','');	
	}

});


/**
 * Color picker
 */
$('.selectablecolor').colpick({
									
								layout:'hex',
								submit:0,
								onChange: function(hsb,hex,rgb,el,bySetColor){


									$('#' + $(el).attr('data-target')).css('background-color','#' + hex);

									var selectedelement = $('span.selected');

									var style = '';
									if(selectedelement.length > 0)
									{
										selectedelement.css('color','');
										style = selectedelement.attr('style');
										

										style = style + ';color: #' + hex + ' !important;';

										selectedelement.attr('style',style);
									}
									else
									{	
										selectedview.css('background-color','');
										style = selectedview.attr('style');
										style = style + ';background-color: #' + hex + ' !important;';

										selectedview.attr('style',style);
									}


								}
							});


/**
 * Event that change element text fontstyle
 * button element
 */
$('.font-style').click(function(){

	var selectedelement = $('span.selected');

	if($(this).hasClass('inverse'))
	{	
		$(this).removeClass('inverse');
		selectedelement.css($(this).attr('data-css'),'');

	}
	else
	{
		$(this).addClass('inverse');
		selectedelement.css($(this).attr('data-css'),$(this).attr('data-value'));
	}
});

/**
 * Event that change element text fontsize
 * input number element
 */
$('#element-text-size').change(function(){

	$('span.selected').css('font-size',$(this).val() + 'px');

});


/**
 * Event that change element text fontfamily
 * select element
 */
$('#element-font-family').change(function(){

	$('span.selected').css('font-family',$(this).val());
});

/**
 * Event that fires click event on file input
 * input element
 */
$('.file-action').click(function(){

	target = $(this).attr('data-target');
	$('#'+ target).trigger('click');

});

/**
 * Event that send selected file for processing for view
 * input element
 */
$('.file-selected').change(function(){

	var input = $(this).attr('id');
	var preview = $(this).attr('data-target');

	PreviewImage(input,preview);

});

/**
 * Event that identifies element on canvas
 * element
 */
$(document).on('click','.view > span.element', function(){

	$('span.selected').removeClass('selected');

	$(this).addClass('selected');

	setElementValuesTools($(this));

});

/**
 * Event that removes image from element and element tool
 * button element
 */
$('.removeimage').click(function(){

	selectedview.find('img#background').removeAttr('src');
	$('#view-image-preview').removeAttr('src')

});

/**
 * Event that remove element from canvas
 * button element
 */
$('.removeelement').click(function(){

	selectedelement = $('span.selected');

	if(selectedelement.length > 0)
	{
		selectedelement.remove();
		$('#' + selectedelement.attr('data-target')).hide();
	}

});

$('.savedesign').click(function(){

	var width = $('.input-credencial-width').val(),
	height = $('.input-credencial-height').val();

	

	var anverso = getElements('anverso');
	var reverso = getElements('reverso');

	

	var data = {};

	if(designeraction == 'save')
	{
		data = {client:selectedclient.id,anverso:anverso,reverso:reverso, width:width, height:height};
	}
	else
	{
		
		data = {client:selectedclient.id,credential:selectedcredential,anverso:anverso,reverso:reverso, width:width, height:height};
	}

	

	var result = ajaxCall('post','clients/savedesign',data);

	if(result.success == true)
	{

		$.Notify({
				caption:  result.caption,
				content:  result.message,
				timeout: 5000,
				style: {background: result.bg,color:result.fg}

			});

		if(designeraction == 'save')
		{

			var data = {client: selectedclient.id};
			selectedcredential = result.credential;
		}
		else
		{
			var data = {client: selectedclient.id,page:selectedpage};
			
		}


		var result = ajaxCall('get','clients/designs',data);

		$('#designscontent').html(result.view);

		designeraction = 'update';

		
	}

	
	
	
});

function resetDesigner()
{
	$('#anversodesigner').css('background-color','#ffffff');
	$('#anversodesigner').find('#background').removeAttr('src');
	$('#anversodesigner').find('span').remove();
	

	$('#reversodesigner').css('background-color','#ffffff');				
	$('#reversodesigner').find('#background').removeAttr('src');
	$('#reversodesigner').find('span').remove();

	$('#element-image-preview').removeAttr('src');
	$('#view-image-preview').removeAttr('scr');
	$('#element-text').val('');
	$('#element-font-family').val('Arial');
	$('#element-font-size').val(10);
	$('#text-color').css('background-color','black');
	$('#view-color').css('background-color','white');
	$('#tools > div').hide();


}

function rebuildDesign()
{
	var data = {credential:selectedcredential};
	var record = ajaxCall('get','clients/designs/getbyid',data);

	$('#anversodesigner').attr('style',record.credential.A_style);

		$('#anversodesigner').width(record.credential.width);
		$('#anversodesigner').height(record.credential.height);
		

		$('.input-credencial-width').val(record.credential.width)
		$('.input-credencial-height').val(record.credential.height)
		

		if(record.credential.A_background){
			$('#anversodesigner').find('#background').attr('src',record.credential.A_background);
		}

		$('#reversodesigner').attr('style',record.credential.R_style);
		$('#reversodesigner').width(record.credential.width);
		$('#reversodesigner').height(record.credential.height);
	
		if(record.credential.R_background){
			$('#reversodesigner').find('#background').attr('src',record.credential.R_background);
		}

		if(record.credential.elements.length > 0){

			for (var i = 0; i < record.credential.elements.length; i++) {

				$('.selected').removeClass('selected');
				
				switch(record.credential.elements[i].data_type)
				{
					case 'text':
						
						$('#'+record.credential.elements[i].view +'designer').append('<span data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'" style="' + record.credential.elements[i].style + '" class="element selected">' +  record.credential.elements[i].textcontent + '</span>');
						$('span.selected').draggable();
						
					break;
					case 'image':
						$('#'+record.credential.elements[i].view+'designer').append('<span data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'"  style="' + record.credential.elements[i].style + '" class="element selected"><img style="width:100%;height:100%;" src="' + record.credential.elements[i].imagecontent +'" /></span>');
						$('span.selected').draggable();
						$('span.selected').resizable();
					break;
					case 'barcode':
						$('#'+record.credential.elements[i].view+'designer').append('<span data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'"  style="' + record.credential.elements[i].style +'" class="element selected"><img style="width:100%;height:100%;" src="' + record.credential.elements[i].imagecontent +'" /></span>');
						$('span.selected').draggable();
						$('span.selected').resizable();
					break;
				}	


			};

		}
}

$('.credential-vertical').on('click', function () {

	$('#anversodesigner').width('204.094px'); //206.094px
	$('#anversodesigner').height('325.039px'); //327.039px

	$('#reversodesigner').width('204.094px'); //206.094px
	$('#reversodesigner').height('325.039px'); //327.039px


	$('.input-credencial-width').val('204.094');
	$('.input-credencial-height').val('325.039');
});

$('.credential-horizontal').on('click', function () {
	$('#anversodesigner').width('325.039px'); //206.094px
	$('#anversodesigner').height('204.094px'); //327.039px

	$('#reversodesigner').width('325.039px'); //206.094px
	$('#reversodesigner').height('204.094px'); //327.039px

	$('.input-credencial-width').val('325.039');
	$('.input-credencial-height').val('204.094');
});

function setElementValuesTools(element)
{

	var target = element.attr('data-target');

	$('#tools > div').hide();
	$('#' + target).show();

	switch(target){
		case 'text-tools':

			$('#element-text').val(element.text());
			$('#element-font-family').val(element.css('font-family'));
			$('#element-text-size').val(element.css('font-size').replace('px',''));
			$('#text-color').css('background-color',element.css('color'));

			$('.font-style').removeClass('inverse');

			if(element.css('font-weight') == 'bold'){
				$('.font-style').slice(0,1).addClass('inverse');
			}

			if(element.css('font-style') != 'normal')
			{
				$('.font-style').slice(1,2).addClass('inverse');
			}

			if(element.css('text-decoration') != 'none')
			{
				$('.font-style').slice(2,3).addClass('inverse');
			}


		break;
		case 'image-tools':

			$('#image-tools > div > img').attr('src', element.find('img').attr('src'));

			if($('span.selected').find('img').attr('data-uri') == 'true')
			{
				$('#image-tools').find('button.file-action').show();
				$('#image-tools').find('button.danger').show();
			}
			else
			{
				$('#image-tools').find('button.file-action').hide();
				$('#image-tools').find('button.danger').hide();
			}

		break;
		case 'format-position-vertical':
			alert()
		break;

	}

}


function addElementToCanvas(option)
{	

	$('span.selected').removeClass('selected');

	if(option.attr('data-type') && selectedview)
	{
		

		switch(option.attr('data-type'))
		{
			case 'text':
				
				var content = ((option.attr('data-fieldname')) ? option.attr('data-fieldname') : 'Texto');
				selectedview.append('<span id="r_' + (elementref++) +'"  data-id="" data-link="' + option.attr('data-id') + '" data-target="text-tools" style="color:#000000;font-family:Arial;font-weight:normal;text-decoration:none;font-style:normal;font-size:10px;position:absolute;cursor:move;" class="element selected" data-type="' + option.attr('data-type') + '">' + content + '</span>');
				
				$('span.selected').draggable();
				$('#element-text').val(content);
				$('#element-font-family').val('Arial');
				$('#element-text-size').val(10);
				$('#text-color').css('background-color','black');
				$('.font-style').removeClass('inverse');

				break;

			case 'image':

				var img = ((option.attr('data-fieldname') ? '<img data-uri="false" style="width:100%;height:100%;" src="../public/img/user_icon.png" />': '<img data-uri="true" style="width:100%;height:100%;" src="../public/img/image.png" />'))
				selectedview.append('<span id="r_' + (elementref++) +'" data-id="" data-link="' + option.attr('data-id') + '" data-target="image-tools" style="position:absolute;cursor:move;width:50px;height:50px;" class="element selected" data-type="' + option.attr('data-type') + '">' + img +'</span>');
				
				$('span.selected').draggable();
				$('span.selected').resizable();

				$('#element-image-preview').attr('src', $('span.selected').find('img').attr('src'));

				if($('span.selected').find('img').attr('data-uri') == 'true')
				{
					$('#image-tools').find('button.file-action').show();
					$('#image-tools').find('button.danger').show();
				}
				else
				{
					$('#image-tools').find('button.file-action').hide();
					$('#image-tools').find('button.danger').hide();
				}

				break;

			case 'barcode':

				selectedview.append('<span id="r_' + (elementref++) +'" data-id="" data-link="' + option.attr('data-id') + '" data-target="barcode-tools" style="background-color:white !important;position:absolute;cursor:move;width:100px;height:50px;" class="element selected" data-type="' + option.attr('data-type') + '"><img data-uri="false" style="width:100%;height:100%;" src="../public/img/barcode.jpg" /></span>');
				$('span.selected').draggable();
				$('span.selected').resizable();

				break;
		}
	}
}


function PreviewImage(inputfile,imgPreview) {
		
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById(inputfile).files[0]);

	oFReader.onload = function (oFREvent) {

		
    	document.getElementById(imgPreview).src = oFREvent.target.result;

    	var input = $('#' + inputfile);

    	if(input.attr('data-type') == 'view')
    	{
    		selectedview.find('img#background').attr('src',oFREvent.target.result);
    		$('#view-image-preview').attr('src',oFREvent.target.result);
    	}
    	else
    	{
    		var selectedelement = $('span.selected');
    		if(selectedelement)
    		{
    			selectedelement.find('img').attr('src',oFREvent.target.result);
    		}

    	}
	
	};

};

function getElements(view)
{


	var data = { 
					background: 		$('#'+view+'designer').find('img#background').attr('src'),
					backgroundstyle: 	$('#'+view+'designer').attr('style'),
					elements: []
				};

	var i = 0;

	$.each($('#'+view+'designer').find('span'),function(){

		var content = '';
		if($(this).attr('data-type') == 'text') { content = $(this).text(); } else { content = $(this).find('img').attr('src'); }

		//

		data.elements[i] = {
				ref:                $(this).attr('id'),
				dataid: 			$(this).attr('data-id'),
				style: 				$(this).attr('style'),
				datatarget: 		$(this).attr('data-target'),
				datatype:   		$(this).attr('data-type'),
				class: 				'element',
				elementcontent: 	content,
				link: 				$(this).attr('data-link')

			}

		i++;

	});

	return data;

}




