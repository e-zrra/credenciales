var item;
var record;
var element;
var optpag;

var recordselected;
var variablefields;
var headers;
var tr;
var arraytd = [];
var dialog;
var choosedesigndialog;
var photobooth;
var photoboothon = 0;


getOrders(null);

$('#datepicker').datepicker({format:"dd-mm-yyyy"});

$('#printcredentials').click(function(){

	 $("#credentialspreview").print({
            globalStyles: true,
            mediaPrint: false,
            stylesheet: null,
            noPrintSelector: ".no-print",
            iframe: true,
            append: null,
            prepend: null,
            manuallyCopyFormValues: true,
            deferred: $.Deferred(),
            timeout: 250
    });

	//$( "#credentialspreview-anverso").print();
	//$.print('#credentialspreview-anverso page');

	/*$( "#credentialspreview-anverso .anverso" ).each(function( data, v) {
		
	});*/

});

$('#printcredentials-inverso').click(function(){

	$("#credentialspreview-inverso").print({
            globalStyles: true,
            mediaPrint: false,
            stylesheet: null,
            noPrintSelector: ".no-print",
            iframe: true,
            append: null,
            prepend: null,
            manuallyCopyFormValues: true,
            deferred: $.Deferred(),
            timeout: 250
    });

	//$( "#credentialspreview-inverso").print();
	//$.print('#credentialspreview-anverso page');

	/*$( "#credentialspreview-anverso .anverso" ).each(function( data, v) {
		
	});*/

});

$('.backtoorderrecords').click(function(e){
	e.preventDefault();

	$('.generatedcredentials').hide();
	$('#orderrecords').show(600);

})

$('#choosedesign').click(function(){

	if(!choosedesigndialog)
	{
		choosedesigndialog = $( "#choosedesigndialog" ).dialog({width: 1000,resizable:false});
	}

	choosedesigndialog.dialog();
	optpag = 'designs';
	

});

$('#markascompleted').click(function(){

	var recordstogenerate = $('#tablerecords > table > tbody > tr');

	var records = {
					table:record.client.category.table,
					items: []
				};

	
	$.each(recordstogenerate,function(){

		var input =  $(this).find('td:eq(0)').find('input');

		if(input.is(':checked'))
		{
			records.items.push(input.attr('data-id'));

		}
	})

	if(records.items.length > 0){

		var data = {records:records};
		var result = ajaxCall('post','orders/records/markascompleted',data);

		if(result.success ==  true)
		{
			$.Notify({
				caption:  result.caption,
				content:  result.message,
				timeout: 5000,
				style: {background: result.bg,color:result.fg}

			});


		$.each(recordstogenerate,function(){

			var input =  $(this).find('td:eq(0)').find('input');

			if(input.is(':checked'))
			{
				$(this).remove();
			}
		})


		}

	}
	else
	{
		$.Notify({
			caption:  'Info...',
			content:  'Seleccione al menos un registro!',
			timeout: 5000,
			style: {background: '#0050ef',color:'white'}

		});
	}

});

$('#generatecredentials').click(function(){

	var recordstogenerate = $('#tablerecords > table > tbody > tr');

	var data = [];

	$.each(recordstogenerate,function(){

		var input =  $(this).find('td:eq(0)').find('input');

		if(input.is(':checked'))
		{
			data.push($(this));
		}
	})

	if(data.length > 0){

		$('#orderrecords').hide();
		$('.generatedcredentials').show();
		generateCredentials(data);
	}
	else
	{
		$.Notify({
			caption:  'Info...',
			content:  'Seleccione al menos un registro a generar!',
			timeout: 5000,
			style: {background: '#0050ef',color:'white'}

		});
	}


});

var queryStringToJSON = function (url) {
    if (url === '')
        return '';
    var pairs = (url || location.search).slice(1).split('&');
    var result = {};
    for (var idx in pairs) {
        var pair = pairs[idx].split('=');
        if (!!pair[0])
            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
    }
    return result;
}

$(document).on('click', 'a.paglink', function (e) {

	e.preventDefault();

	if(!$(this).parent().hasClass('disabled')){

		switch(optpag){
			case 'designs' :

				var string = $(this).attr('href').split('designs')[1];

				var json = queryStringToJSON(string);

				selectedpage = $(this).text();

				json.client = record.client_id;
				var result = ajaxCall('get','clients/designs',json);

				$('#designscontent').html(result.view);
				$(document).find('.showdesigner > i').removeClass('icon-pencil').addClass(' icon-checkmark');

			break;
			case 'records' :
				var string = $(this).attr('href').split('getrecords')[1];

				var json = queryStringToJSON(string);

				json["order"] = record.id;
				json["perpage"] = $('#recordperpage').val();
				json["status"]  = $('#recordstatus').val();
				json["search"]  = $('#recordsearch').prev().val();

				
		       
		        result = ajaxCall('get','orders/getrecords',json);
				

				updateRecordsTable(result);
				
				$('#pag').html(result.view);
			break;

		}

		
	}

   

});

$(document).on('click','.removerecord',function(){

	
	var data = new FormData();

	data.append('id',$(this).attr('data-id'));
	data.append('table',record.client.category.table);
	data.append('order',record.id);

	$.each(headers,function(){

		if($(this).attr('data-type') =='image')
		{
			data.append('imagefield',$(this).attr('id').substring(3));
			return false;

		}
	});

	var result = ajaxCall2('post','orders/record/remove',data,false,false);

	if(result.success == true)
	{
		$(this).parent().parent().remove();
	}

	$.Notify({
			caption:  result.caption,
			content:  result.message,
			timeout: 5000,
			style: {background: result.bg,color:result.fg}

		});

	
});

$('#downloadexcel').click(function(e){

	e.preventDefault();

	var url = $(this).attr('href');

	var location = url + '?id=' + record.credential.id;

	window.location.href = location;

});


$('#closephoto').on('click',function(){

	photobooth.data( "photobooth" ).pause();
	photoboothon = 0;
	dialog.dialog('close');
});

$( '#photo' ).on( "image", function( event, dataUrl ){
	 
	$.each(headers,function(){

		if($(this).attr('data-type') == 'image')
		{	
			$('.recordtoedit').find('td:eq(' + $(this).index() +')').find('div img').attr('src',dataUrl);
			return false;
		}

	})

});

				

$(document).on('click','.opencamera',function(){
	//alert()

	if($(this).parent().parent().hasClass('recordtoedit')){




		if(!dialog){

			dialog = $( "#dialog" ).dialog({width: 400,resizable:false});
			
			if(!photobooth)
			{

				photobooth = $('#photo').photobooth();
				var ul = $('#photo ul');
				ul.css('border-bottom-left-radius','0px');
				ul.css('top','-9px');
				ul.css('width','48px');
				ul.css('list-style-type','none');

				photoboothon = 1;
			}

		}
		else
		{
			dialog.dialog('open');
			if(photoboothon == 0)
			{
				photobooth.data( "photobooth" ).resume();
				photoboothon = 1;
			}	

		}

		

	}
	

});

$( '#cameracontainer' ).on( "image", function( event, dataUrl ){
	 $('#formimgpreview').attr('src',dataUrl);
});


$('#recordstatus').change(function(){

	getRecords();
})


$('#recordperpage').change(function(){
	
	getRecords();

});

$('#recordsearch').click(function(){

	getRecords();
});


$('#order-records').click(function(){

	$('#main').hide();
	$('#new').hide();
	$('#orderrecords').show(600); 


	if(record.id != recordselected ){

		getRecords();
		
	}

});


var loads = [];

var index;//= evt.parent().index();

var field; //= evt.attr('field');

$('body').delegate('th .update_image_by_row', 'change', function () {

	index = $(this).parent().index();

	field = $(this).attr('field');

	loads = [];

	openFiles($('#update_image_by_row'));
});

function sortArrOfObjectsByParam(arrToSort /* array */, strObjParamToSortBy /* string */, sortAscending /* bool(optional, defaults to true) */) {
    if(sortAscending == undefined) sortAscending = true;  // default to true
    
    if(sortAscending) {
        arrToSort.sort(function (a, b) {
            return a[strObjParamToSortBy] > b[strObjParamToSortBy];
        });
    }
    else {
        arrToSort.sort(function (a, b) {
            return a[strObjParamToSortBy] < b[strObjParamToSortBy];
        });
    }
}

$('.save-data-load').on('click', function () {

	if (!loads || loads.length == 0) {
		$.Notify({
			caption:  'Info...',
			content:  '¡No se ha seleccionado imagenes!',
			timeout: 5000,
			style: {background: '#0050ef',color:'white'}

		});
		return false;
	}

	var recordstogenerate = $('#tablerecords > table > tbody > tr');

	var checked = false;

	$.each(recordstogenerate,function(){ //tr

		var tr = $(this);


		var data = new FormData();

		var data_id = tr.find('.saverecord');
		

		data.append('recordid',data_id.attr('data-id'));
		data.append('tablename',record.client.category.table);
		data.append('order',record.id);

		$.each(headers,function(){

			var th = $(this);

			/*if($(this).attr('data-type') == 'text')
			{	
				
				data.append([th.attr('id').substring(3)], tr.find('td:eq(' + th.index() +')').find('input').val());
			}*/

			if($(this).attr('data-type') == 'image')
			{
				var src = tr.find('td:eq(' + th.index() +')').find('div img').attr('src');
				
				if(src){

					if(src.indexOf('data:image') >= 0 ){

						data.append([th.attr('id').substring(3)], tr.find('td:eq(' + th.index() +')').find('div img').attr('src'));
						data.append('imagefield', th.attr('id').substring(3));
						//
					}
				}
			}

		});

		var result = ajaxCall2('post','orders/record/update',data,false,false);

		if(result.success == true)
		{
			//
		}

		/*if(result.success == true)
		{

			for (var i = 0; i < arraytd.length; i++) {
			
				var input = arraytd[i].find('input');

				arraytd[i].text(input.val());
				input.remove();

			};

			//$('.recordtoedit').removeClass('recordtoedit');


		}*/

		
	});

	$.Notify({
		caption:  'Información',
		content:  'Se actualizo los registros',
		timeout: 5000,
		style: {background: 'rgb(96, 169, 23)',color:'white'}
	});

	/*

	if($(this).parent().parent().hasClass('recordtoedit'))
	{

		var parent = $(this).parent().parent();

		var data = new FormData();

		data.append('recordid',$(this).attr('data-id'));
		data.append('tablename',record.client.category.table);
		data.append('order',record.id);

		$.each(headers,function(){

			var th = $(this);

			if($(this).attr('data-type') == 'text')
			{	
				
				data.append([th.attr('id').substring(3)], parent.find('td:eq(' + th.index() +')').find('input').val());
			}

			if($(this).attr('data-type') == 'image')
			{
				var src = parent.find('td:eq(' + th.index() +')').find('div img').attr('src');
				//
				if(src){

					if(src.indexOf('data:image') >= 0 ){

						data.append([th.attr('id').substring(3)], parent.find('td:eq(' + th.index() +')').find('div img').attr('src'));
						data.append('imagefield', th.attr('id').substring(3));
					}
				}
			}

		});

		var result = ajaxCall2('post','orders/record/update',data,false,false);

		if(result.success == true)
		{

			for (var i = 0; i < arraytd.length; i++) {
			
				var input = arraytd[i].find('input');

				arraytd[i].text(input.val());
				input.remove();

			};

			$('.recordtoedit').removeClass('recordtoedit');


		}


		//$.Notify({
		//	caption:  result.caption,
		//	content:  result.message,
		//	timeout: 5000,
		///	style: {background: result.bg,color:result.fg}
		//});
		
	}*/

})

function sortProperties(obj)
{
  // convert object into array
    var sortable=[];
    for(var key in obj)
        if(obj.hasOwnProperty(key))
            sortable.push([key, obj[key]]); // each item is an array in format [key, value]

    // sort items by value
    sortable.sort(function(a, b)
    {
        var x=a[1].toLowerCase(),
            y=b[1].toLowerCase();
        return x<y ? -1 : x>y ? 1 : 0;
    });
    return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
}

var sortBy = function (key) {
    return function (o, p) {
        var a, b;
        if (o && p && typeof o === 'object' && typeof p === 'object') {
            a = o[key];
            b = p[key];
            if (a === b) {
                return 0;
            }
            if (typeof a === typeof b) {
                return a < b ? -1 : 1;
            }
            return typeof a < typeof b ? -1 : 1;
        }
    }        
};

$('.show-data-load').on('click', function () {

	loads.sort(sortBy('name'));

	

	if (!loads || loads.length == 0) {
		$.Notify({
			caption:  'Info...',
			content:  '¡No se ha seleccionado imagenes!',
			timeout: 5000,
			style: {background: '#0050ef',color:'white'}

		});
		return false;
	}
	
	var recordstogenerate = $('#tablerecords > table > tbody > tr');

	var checked = false;

	$.each(recordstogenerate,function(){

		var input =  $(this).find('td:eq(0)').find('input');

		if(input.is(':checked'))
		{
			checked = true;

			return false;
		}
	});
	
	if (!checked) {
		$.Notify({
			caption:  'Info...',
			content:  '¡Seleccione al menos un registro!',
			timeout: 5000,
			style: {background: '#0050ef',color:'white'}

		});
		return false;
	}

	$.each(loads, function (i, v) {

		var tr = $('#tablerecords tr:eq('+i+')');

		var input =  tr.find('td:eq(0)').find('input');

		if(input.is(':checked'))
		{
			var td = tr.find('td:eq('+index+')');

	    	td.find('div img').attr('src', '');

	    	td.find('div img').attr('src', v.data);
		}
	});
})

function openFiles(evt) {

	var tgt = evt.target || window.event.srcElement, files = tgt.files;

    //var files = evt.target.files;

    for (var i = 0; i < files.length; i++) {
        var file = files[i],
            reader = new FileReader();

        reader.onload = (function (file) { 
            return function (e) { 
                var data = this.result;

                loads.push({
                    data: data,
                    name: file.name
                });
            }
        })(file);

        reader.readAsDataURL(file);
    }
}

function handleFilesChange(evt) {

	//

	var index = evt.parent().index();

	var tgt = evt.target || window.event.srcElement, files = tgt.files;

    var field = evt.attr('field');

    var count = files.length;
    
    var content_images = $('#content-images');

    for (var i =0;i < count; i++) {

    	var f = files[i];

    	var tr = $('#tablerecords tr:eq('+i+')');

    	var td = tr.find('td:eq('+index+')');
    	
    	 var fileUrl = window.URL.createObjectURL(f);

        //

        td.find('div img').attr('src', fileUrl);

    	/*var reader = new FileReader();
	    
	    reader.onloadend = readSuccess;                                            
	    
	    function readSuccess(evt) { 

	    	                            
	    };

	    reader.readAsText(f);*/

    	//reader.readAsDataURL(f);

    	//

	    //reader.onload = function (e) {
             //$("<input class='content-img-update' value='val' />", {
                 //"src": e.target.result,
                 //"class": 'img'
             //}).appendTo(content_images);

			

             //alert(e.target.result);
             //$('#data-image-update').val('a');
         //}

         

        //jQuery(td).find('div img').attr('src', img);

    }

    

    /*$.each($('#content-images input'), function () {
    	
    });*/

}

function print_data () {
	
} 

function image_source (file, onLoadCallback) {

	/*var reader = new FileReader();
    reader.onload = onLoadCallback;
    reader.readAsText(file);*/

    //


	/*var reader = new FileReader();

	reader.onload = function (e) {

		img =  e.target.result;

		return function () {
			
		}
		
    }

    reader.readAsDataURL(f);*/

    ///
}


$(document).on('click','.saverecord',function(){

	if($(this).parent().parent().hasClass('recordtoedit'))
	{

		var parent = $(this).parent().parent();

		var data = new FormData();

		data.append('recordid',$(this).attr('data-id'));
		data.append('tablename',record.client.category.table);
		data.append('order',record.id);

		$.each(headers,function(){

			var th = $(this);

			if($(this).attr('data-type') == 'text')
			{	
				
				data.append([th.attr('id').substring(3)], parent.find('td:eq(' + th.index() +')').find('input').val());
			}

			if($(this).attr('data-type') == 'image')
			{
				var src = parent.find('td:eq(' + th.index() +')').find('div img').attr('src');
				//
				if(src){

					if(src.indexOf('data:image') >= 0 ){

						data.append([th.attr('id').substring(3)], parent.find('td:eq(' + th.index() +')').find('div img').attr('src'));
						data.append('imagefield', th.attr('id').substring(3));
					}
				}
			}

		});

		var result = ajaxCall2('post','orders/record/update',data,false,false);


		if(result.success == true)
		{

			for (var i = 0; i < arraytd.length; i++) {
			
				var input = arraytd[i].find('input');

				arraytd[i].text(input.val());
				input.remove();

			};

			$('.recordtoedit').removeClass('recordtoedit');


		}


		$.Notify({
			caption:  result.caption,
			content:  result.message,
			timeout: 5000,
			style: {background: result.bg,color:result.fg}

		});
		
	}

});


$(document).on('click','.editrecord',function(){



	if(tr != $(this)){


		for (var i = 0; i < arraytd.length; i++) {
			
			var input = arraytd[i].find('input');

			arraytd[i].text(input.val());
			input.remove();

		};


	}	


	tr = $(this).parent().parent();
	
	$('#tablerecords > table > tbody > tr').removeClass('recordtoedit');

	tr.addClass('recordtoedit');
	var td = tr.find('td').slice(1);

	arraytd = [];
	var i = 0;

	$.each(td,function(){

		if($(this).attr('data-type') != 'image')
		{
			arraytd[i] = $(this);

			var input = $(this).find('input');

			if(input.length>0)
			{
				$(this).html(input.val())
			}
			else
			{
				$(this).html('<input type="text" value="' + $(this).text() + '">');
				
			}
			i++;
		}
		
	});


	
});


$('#uploadexcel').click(function(){

	$('#excelfile').trigger('click');

});

$('#excelfile').change(function(){


	var data = new FormData();
	data.append("file", document.getElementById('excelfile').files[0]);
	data.append('credential',record.credential.id);
	data.append('order',record.id);

	array = [];

	$.each(headers.slice(1),function(){
		
		if($(this).attr('data-type') != 'image')
		{
			var column = $(this).attr('id');
			array.push((column).replace('th_',''));
		}
	});

	//

	data.append('columns',array);

	result = ajaxCall2('post','orders/uploadexcel',data,false,false);

	$.Notify({
			caption:  result.caption,
			content:  result.message,
			timeout: 5000,
			style: {background: result.bg,color:result.fg}

	});


	if(result.success == true)
	{
		
		$('#order-records').trigger('click');
		getRecords();
		$(this).val('');
	}

	

});

$('#new').click(function(){
	
	$('#main').hide();
	$('#order').show(600);
	$('#form-order')[0].reset();
	$('#form-order').find('#action-order').attr('data-action','save').text('Registrar');
	$(this).hide();

});

$('#cancel').click(function(){
	

	$('#order').hide();
	$('#main').show(600);
	$('#form-order').find('#action-order').attr('data-action','');
	
	$('#new').show();


});

$('#action-order').click(function(){

	var data 	= $('#form-order').serializeArray(); 
	var result;

	$(this).attr('disabled','disabled');

	if($(this).attr('data-action') == 'save')
	{
		result 	= ajaxCall('post','orders/new',data);
		getOrders();
	}
	else
	{
		data.push({'name':'id','value':item});
		result 	= ajaxCall('post','orders/update',data);
		refreshOrderInfo(result);	
	}

	$.Notify({
			caption:  result.caption,
			content:  result.message,
			timeout: 5000,
			style: {background: result.bg,color:result.fg}

	});


	if(result.success == true)
	{

		$('#form-order')[0].reset();
		$('#order').hide();
		$('#main').show(600);
		$('#form-order').find('#action-client').attr('data-action','');
		$('#new').show();
		
	}

	$(this).removeAttr('disabled');
		
});

$(document).on('click','.showdesigner',function(){

	//if(record.credential.id != $(this).attr('data-id')){
		var anverso = $('#anversopreview').html();
		var anversostyle =  $('#anversopreview').attr('style');

		var reverso = $('#reversopreview').html();
		var reversostyle =  $('#reversopreview').attr('style');

		var data = {order:record.id,design:$(this).attr('data-id')};

		var result = ajaxCall('post','orders/setdesign',data);

		if(result.success == true)
		{
			$('#anverso').html(anverso);
			$('#anverso').attr('style',anversostyle);

			$('#reverso').html(reverso);
			$('#reverso').attr('style',reversostyle);

			choosedesigndialog.dialog('close');
			$('#choosedesign').next().show();

			$('#anverso').show();
			$('#reverso').hide();

			record.credential = result.credential;
			updateHeaderTable();

		}
		
		$.Notify({
				caption:  result.caption,
				content:  result.message,
				timeout: 5000,
				style: {background: result.bg,color:result.fg}

		});
	//}
	//else
	//{
	//	choosedesigndialog.dialog('close');
	//}
});


$(document).on('click','a.list',function(e){

	e.preventDefault();

	if(item != $(this).attr('data-id'))
	{
		item  = $(this).attr('data-id');
		$('#details').show();
		$('#credential').hide();

		$(this).addClass('active');
		$('.list').not($(this)).removeClass('active');

		var data = {id:$(this).attr('data-id')};
		var result = ajaxCall('get','orders/getbyid',data);

		setOrderDetails(result);

		record = result.record;
		updateHeaderTable();
		

		var data = {client:record.client_id};
		var result = ajaxCall('get','clients/designs',data);
		$('#designscontent').html(result.view);

		$(document).find('.showdesigner > i').removeClass('icon-pencil').addClass(' icon-checkmark');

		if(!record.credential)
		{
			$('#alertcredential').html('No se Ha definido Diseño');
			$('#alertcredential').next().next().hide();

			resetDesigner();
		}
		else
		{
			$('#alertcredential').html('');
			$('#alertcredential').next().next().show();



			resetDesigner();
			rebuildDesign();
			$('#anverso').show();
			$('#reverso').hide();
		}
	
	}

});

$('.showpreview').click(function(){

	var target = $(this).attr('data-target');
	var hide = $(this).attr('data-hide');

	$('#' + target).show(400);
	$('#' + hide).hide();

});

$('#order-credential').click(function(){
	
	$('#details').hide();
	$('#credential').show(600);

});

$('#backtodetails').click(function(e){

	e.preventDefault();
	$('#credential').hide();
	$('#details').show(600);


});

$('.backtomain').click(function(){

	$('#designer').hide();
	$('#orderrecords').hide();
	$('#main').show(600);
	$('#new').show();

});



$('#order-edit').click(function(){

	$('#main').hide();
	$('#order').show(600);
	$('#form-order').find('#action-order').attr('data-action','update').text('Actualizar');
	$('#new').hide();

	var data 	= {id:item};
	var result 	= ajaxCall('get','orders/getbyid',data);

	setOrderInfoIntoForm(result);

});

$('#order-cancel').click(function(){

	var data = {id:item};
	var result = ajaxCall('post','orders/cancel',data);


	if(result.status)
	{
		$('#details').hide();
		$('a.list.active').parent().remove();
	}

	$.Notify({
			caption:  result.caption,
			content:  result.message,
			timeout: 5000,
			style: {background: result.bg,color:result.fg}

	});

});

$('#order-approve').click(function(){

	var data = {id:item};
	var result = ajaxCall('post','orders/approve',data);

	if(result.status)
	{
		var a = $('.active');

		a.find('.list-title').find('span').removeClass().addClass('place-right icon-flag-2 ' + result.status.class + ' smaller' );
		a.find('.list-subtitle').find('span').text(result.status.description);
	}

	$.Notify({
			caption:  result.caption,
			content:  result.message,
			timeout: 5000,
			style: {background: result.bg,color:result.fg}

	});


});

$('#order-remove').click(function(){


	if (confirm("Seguro Desea Eliminar La Solicitud Seleccionada?") == true) {
        var data = {id:item};

		var result = ajaxCall('post','orders/remove',data);

		if(result.success == true)
		{

			$('#details').hide();
			$('a.list.active').parent().remove();
		}

		$.Notify({
				caption:  result.caption,
				content:  result.message,
				timeout: 5000,
				style: {background: result.bg,color:result.fg}

		});
    } 

});

$('#ordersearch').click(function(){

	item = null;
	var data 	= getSearchParemeters();
	getOrders(data);

});

$('#status-search').change(function(){
	item = null;
	var data 	= getSearchParemeters();
	getOrders(data);
});

function getBarcodeGenerator()
{
	
	for (var i = 0; i < record.credential.elements.length; i++) {
		
		if(record.credential.elements[i].data_type == 'text')
		{
			if(record.credential.elements[i].field != null){

				if(record.credential.elements[i].field.generates == 'barcode')
				{
					var generator = record.credential.elements[i].field.fieldname;
					
					return generator;
				}


			}

		}
	};

}


function generateCredentials(data)
{
	
	$('#credentialspreview').html('');

	var array_barcode_field = [];

	$('#content-setting-codebar ul li').each(function () {

		var field = $(this).find('input');

		if(field.is(':checked')) {

			array_barcode_field.push(field.val())

		}
	});



	var barcodegen = getBarcodeGenerator();

	$.each(data,function() {

		var html = '';
		
		html += $('#previews').html();

		var page = '<page size="A4" class="last current"></page>';

		$('#credentialspreview').append(page);

		$('.last').append(html);
		$('.last').removeClass('last');

		$('#credentialspreview').find('#anverso').css('border','').css('margin','').show();
		$('#credentialspreview').find('#reverso').css('border','').css('margin','').show();

		var barcode_g = ''; 

		$.each($(this).find('td'),function() {

			var _field = $(this).attr('data-field');

			if (_field != undefined) {

				if (array_barcode_field.indexOf(_field) > -1) {

					//console.log( _field )
					var value_field = $(this).text();
					barcode_g += value_field;
				};
			};

			/*if (_field != 'undefined') {
				if (array_barcode_field.indexOf(_field)) {
					var value_field = $(this).text();
					barcode_g += _field;
				};
			};*/

			if($(this).attr('data-field'))
			{
				if($(this).attr('data-type') == 'text'){
					$('.current').find('span[data-field="' + $(this).attr('data-field') + '"]').text($(this).text());
				}

				if($(this).attr('data-type') == 'image')
				{
					$('.current').find('span[data-field="' + $(this).attr('data-field') + '"]').find('img').attr('src',$(this).find('img').attr('src') + '?id' + new Date().getTime() );
				}
			}
		});

		$('.current').find('span[data-type="barcode"]').find('img').JsBarcode( barcode_g );
		
		//console.log('Codigo de barras:' + barcodegen)//barcode_g

		$('.current').removeClass('current');
		
	});

	
}

function resetDesigner()
{
	$('#anverso').css('background-color','#ffffff');
	$('#anverso').find('#background').removeAttr('src');
	$('#anverso').find('span').remove();
	
	$('#reverso').css('background-color','#ffffff');				
	$('#reverso').find('#background').removeAttr('src');
	$('#reverso').find('span').remove();

}


function rebuildDesign()
{

	$('#anverso').attr('style',record.credential.A_style);
			
	if(record.credential.A_background){
		$('#anverso').find('#background').attr('src',record.credential.A_background);
	}

	$('#reverso').attr('style',record.credential.R_style);

	if(record.credential.R_background){
		$('#reverso').find('#background').attr('src',record.credential.R_background);
	}

	if(record.credential.elements.length > 0){

		for (var i = 0; i < record.credential.elements.length; i++) {

			$('.selected').removeClass('selected');
			
			switch(record.credential.elements[i].data_type)
			{
				case 'text':
					
					$('#'+record.credential.elements[i].view).append('<span data-field="'+((record.credential.elements[i].field) ? record.credential.elements[i].field.fieldname : '')+'" data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'" style="' + record.credential.elements[i].style + '" class="element selected">' +  record.credential.elements[i].textcontent + '</span>');
					
					
				break;
				case 'image':
					$('#'+record.credential.elements[i].view).append('<span data-field="'+((record.credential.elements[i].field) ? record.credential.elements[i].field.fieldname : '')+'" data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'"  style="' + record.credential.elements[i].style + '" class="element selected"><img style="width:100%;height:100%;" src="' + record.credential.elements[i].imagecontent +'" /></span>');
					
				break;
				case 'barcode':
					$('#'+record.credential.elements[i].view).append('<span data-id="' + record.credential.elements[i].id + '" data-link="'+ record.credential.elements[i].link_id+'" data-target="'+record.credential.elements[i].data_target+'" data-type="'+record.credential.elements[i].data_type+'"  style="' + record.credential.elements[i].style +'" class="element selected"><img style="width:100%;height:100%;" src="' + record.credential.elements[i].imagecontent +'" /></span>');
					
				break;
			}	


		};

	}
}

function getRecords() {

	searchfields = [];

	if(headers)
	{
		$.each(headers,function(){

			var th = $(this);

			if($(this).attr('data-type') == 'text' && $(this).find('input').is(':checked'))
			{	
				searchfields.push(th.attr('id').substring(3));
			}
		});
	}

	var data = {
			order: 		record.id,
			perpage: 	$('#recordperpage').val(),
			status: 	$('#recordstatus').val(),
			search: 	$('#recordsearch').prev().val(),
			searchfields: searchfields
		};

	result = ajaxCall('get','orders/getrecords',data);
	
	recordselected = record.id;

	updateRecordsTable(result);
	
	$('#pag').html(result.view);
}

$('#tablerecords').delegate('.checked-all-checkboxes', 'change', function () {
	if($(this).prop('checked')){
        $('#tablerecords tbody tr td input[type="checkbox"]').each(function(){
            $(this).prop('checked', true);
        });
    }else{
        $('#tablerecords tbody tr td input[type="checkbox"]').each(function(){
            $(this).prop('checked', false);
        });
    }
});
$('#btn-setting-codebar').on('click', function () {
	$('#content-setting-codebar').removeClass('hidden');
});


function updateHeaderTable()
{

	var head = $('#tablerecords > table > thead');	
	head.html('');

	
	if(record.credential){

		var html = '<th width="200px"><input type="checkbox" class="checked-all-checkboxes" name=""></th>';

		variablefields = record.credential.elements;

		var content_barcode = $('#content-setting-codebar');

		for (var i = 0; i < variablefields.length; i++) {

			//console.log(variablefields[i])
			
			if(variablefields[i].link_id != 0) {

				if(variablefields[i].data_type == 'text'){

					content_barcode.find('ul').append('<li><label><input value="'+variablefields[i].field.fieldname+'" id="codebar_item_'+variablefields[i].field.fieldname+'" type="checkbox" /> '+variablefields[i].field.displayname+'</label></li>');


					html += '<th data-type="' + variablefields[i].data_type + '" id="th_' + variablefields[i].field.fieldname + '"><input type="checkbox"> ' + variablefields[i].field.displayname + '</th>';
				}
				else
				{

					html += '<th data-type="' + variablefields[i].data_type + '" id="th_' + variablefields[i].field.fieldname + '">' + variablefields[i].field.displayname + ' | <input id="update_image_by_row" class="update_image_by_row" type="file" field="'+variablefields[i].field.fieldname+'" multiple></th>';
				}


			}
		}

		head.html(html);
		headers = $('#tablerecords > table > thead > th');
	}
}

function updateRecordsTable(result)
{
	var head = $('#tablerecords > table > thead');
	var body = $('#tablerecords > table > tbody');

	//head.html('');
	
	body.html('');

	//

	if(record.credential){

		head.html(html);
		
		var html = '';

		for (var i = 0; i < result.records.length; i++) {
			
			html+='<tr>';

			$.each(headers,function(key,value){

				if($(this).attr('data-type') == 'image')
				{
					html += '<td data-type="image"><div style="width:74px;height:70px;border:1px solid gray;"><img style="width:100%;height:100%;" ></div></td>';
				
				}else{

					html += '<td data-type="text"></td>';	
				}
			});

			html+='</tr>';

			body.append(html);//crea una fila completa
			
			html ='';

			var element = result.records[i]; //fila completa

			

			$.each(element,function(index,data){

				if(index.indexOf('field') != -1 && data != null)
				{
					var th = $('#th_' + index);

					var td = body.find('tr:last').find('td:eq(' + th.index() +')');


					if(td.attr('data-type') === 'image')
					{
						//alert(data)
						td.find('div img').attr('src',data + '?id=' + new Date().getTime() );
							
						td.attr('data-field',index);
					}
					else
					{
						td.text(data);
						td.attr('data-field',index);
					}
				}

			});

			body.find('tr:last').find('td:eq(' + 0 +')').html('<div class="input-control checkbox"><label><input type="checkbox" data-id="'+ result.records[i].id +'" /><span class="check"></span></label></div><button class="warning mini"><i class="icon-checkmark"></i></button> <button data-hint="Guardar|Guardar los cambios" data-hint-position="top" data-id="' + result.records[i].id + '" class="success saverecord mini"><i class="icon-floppy"></i></button> <button data-hint="Fotografía|Capturar fotografía" data-hint-position="top" class="default opencamera mini"><i class="icon-camera"></i></button> <button data-hint="Editar|Modificar Registro" data-hint-position="top"  class="primary mini editrecord"><i class="icon-pencil"></i></button> <button data-id="' + result.records[i].id + '" data-hint="Eliminar|Eliminar Registro" data-hint-position="top" class="danger mini removerecord"><i class="icon-remove"></i></button>');

		};
	}

}


function PreviewImage(inputfile,imgPreview,type) {

	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById(inputfile).files[0]);

	oFReader.onload = function (oFREvent) {

		
    	document.getElementById(imgPreview).src = oFREvent.target.result;

    	if(type == 'view'){
    		containment.find('img#background').attr('src',oFREvent.target.result)
    	}else{

    		$('.selected').find('img').attr('src',oFREvent.target.result)
    		$('.selected').find('img').attr('data-uri',true);
    		
    	}
 	 		
	};

};



function getSearchParemeters()
{
	var data = {status:$('#status-search').val(),search:$('#text-search').val()}
	return data;
}

function refreshOrderInfo(data)
{
	var a = $('.active');

	a.find('.list-subtitle').text(data.record.client.name);
	a.find('.list-subtitle').append('<span class="place-right">' + data.record.status.description + '</span>');
	a.find('.list-remark').text('Credenciales: ' + data.record.noitems);
	
	$('#order-noitems').text('No de Credenciales Solicitadas: ' + data.record.noitems);
	$('#order-client').text('Cliente: ' + data.record.client.name);
	$('#order-duedate').text('Fecha Requerida de  Entrega: ' + data.record.duedate);
	$('#order-notes').text(data.record.notes);

}


function setOrderInfoIntoForm(data)
{
	var form = $('#form-order');

	form.find('select[name=client]').val(data.record.client_id);
	form.find('input[name=noitems]').val(data.record.noitems);

	var date = new Date(data.record.duedate);
	var properlyFormatted = ("0" + date.getUTCDate()).slice(-2) + '-'+ ("0" + (date.getUTCMonth() + 1)).slice(-2) + '-' + date.getUTCFullYear();
	
	form.find('input[name=duedate]').val(properlyFormatted);
	form.find('textarea').val(data.record.notes);

	
}

function setOrderDetails(data)
{
	$('#order-code').text('No de Solicitud: ' + data.record.code);
	$('#order-noitems').text('No de Credenciales Solicitadas: ' + data.record.noitems);
	$('#order-itemsremaining').text('No de Credenciales Restantes por Entregar: ');
	$('#order-client').text('Cliente: ' + data.record.client.name);
	$('#order-duedate').text('Fecha Requerida de Entrega: ' + data.record.duedate);
	$('#order-dateadded').text('Fecha de Registro: ' + data.record.created_at);
	$('#order-notes').text('Notas: ' + data.record.notes);
}


function getOrders(data)
{
	result = ajaxCall('get','orders/get',data);
	$('#orders').html('').html(result.view);
	$('#details').hide();
	$('#credential').hide();
}

function ajaxCall(type,url,data)
{
	var result;

	$.ajax({
			type: type,
			url: url,
			data: data,
			async:false
	})
  	.done(function( msg ) {
    	
  		result = msg;

  	});

  	return result;

}

function ajaxCall2(type,url,data,processData,contentType)
{
	var result;

	$.ajax({
			type: type,
			url:  url,
			data: data,
			cache: false,
			contentType: contentType,
			processData: processData,
			async:false
	})
  	.done(function( msg ) {
    	
  		result = msg;

  	});

  		return result;

}